package com.kwtaApp.kwta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.kwtaApp.kwta.ui.views.Login;

import java.io.File;

public class SplashActivity extends AppCompatActivity {
    private String getAppPath() {
        try {
            final File filesDir = getApplicationContext().getFilesDir();
            if (filesDir != null) {
                return filesDir.getAbsolutePath();
            }
            //  Log.w(ACRA.LOG_TAG, "Couldn't retrieve ApplicationFilePath for : " + context.getPackageName());
            return "Couldn't retrieve ApplicationFilePath";
        } catch (Exception ex) {
            return "Error trying to read file:\n" + ex.toString();
        }

    }


//Checks Internet Availability
        private  final String TAG = SplashActivity.class.getSimpleName();



        public  boolean isInternetAvailable(Context context)
        {
            NetworkInfo info = (NetworkInfo) ((ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

            if (info == null)
            {
                Log.d(TAG,"no internet connection");
                Toast.makeText(SplashActivity.this ,"No internet availability" ,Toast.LENGTH_LONG).show();
                return false;
            }
            else
            {
                if(info.isConnected())
                {
                    Log.d(TAG," internet connection available...");
                    Toast.makeText(SplashActivity.this ,"This shows internet availability" ,Toast.LENGTH_LONG).show();

                    return true;
                }
                else
                {
                    Log.d(TAG," internet connection");
                    Toast.makeText(SplashActivity.this ,"No internet availability" ,Toast.LENGTH_LONG).show();
                    return true;
                }

            }
        }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
       // Toast.makeText(getApplicationContext() ,"The file path of the app"  +getAppPath(),Toast.LENGTH_LONG).show();
        getAppPath();
        //isInternetAvailable(this);



        new Handler().postDelayed(new Runnable() {
        // Using handler with postDelayed called runnable run method
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, Login.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, 5*1000);

    }
}