package com.kwtaApp.kwta;

import com.android.volley.toolbox.StringRequest;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "https://www.futuristickwtadev.com/index.php/api/login_user";
    public static String URL_SURVEY = "https://www.futuristickwtadev.com/index.php/api/list_surveys";
    public static String URL_QUESTIONS = "https://www.futuristickwtadev.com/index.php/957141";
}
