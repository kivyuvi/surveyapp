package com.kwtaApp.kwta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.TabAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;

public class AllSaveResponses extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    private SQLiteHandler db;

    private String syncedData;
    private String unsyncedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_save_responses);

        //set title
        AllSaveResponses.this.setTitle("SAVED RESPONSES");

        db = new SQLiteHandler(getApplicationContext());

        HashMap<String, String> allResponses = db.getAllAnswers();
        try {
            if(allResponses.get("saved") == null){
                unsyncedData = "0";
            }else {
                JSONArray sArr = new JSONArray(allResponses.get("saved"));
                unsyncedData = String.valueOf(sArr.length());
                Toast.makeText(getApplicationContext(),"There are "+unsyncedData +"unsynced responses" ,Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HashMap<String, String> syncedResponses = db.getSyncedAnswers();
        try {
            if(syncedResponses.get("saved") == null){
                syncedData = "0";
            }else {
                JSONArray sArr = new JSONArray(allResponses.get("saved"));
                syncedData = String.valueOf(sArr.length());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
        viewPager=(ViewPager)findViewById(R.id.viewPager2);



        tabLayout.addTab(tabLayout.newTab().setText("Not Synced" + " (" +unsyncedData+ ") "));
        tabLayout.addTab(tabLayout.newTab().setText("Synced" + " (" +syncedData+ ") "));

        final TabAdapter adapter = new TabAdapter(this,getSupportFragmentManager(), 2);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}