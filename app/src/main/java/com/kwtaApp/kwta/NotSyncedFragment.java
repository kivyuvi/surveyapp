package com.kwtaApp.kwta;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.ResponseMethods;
import com.kwtaApp.kwta.ui.adapter.ResponsesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotSyncedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotSyncedFragment extends Fragment implements ResponsesAdapter.onGasListener  {

    private  String TAG = "Answers";
    private SQLiteHandler db;
    private RecyclerView savedResponses;
    private ProgressDialog pDialog;

    // response methods and adapter
    private ArrayList<ResponseMethods> responses = new ArrayList<>();
    private ResponsesAdapter responsesAdapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NotSyncedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotSyncedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotSyncedFragment newInstance(String param1, String param2) {
        NotSyncedFragment fragment = new NotSyncedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_not_synced, container, false);
        db = new SQLiteHandler(getContext());
        String data = db.getAllAnswers().toString();
        //Log.d("db",data);
        getSavedResponses();

        savedResponses = rootView.findViewById(R.id.notsynced_recycler);
        initRecyclerView();
        return  rootView;
    }
    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        savedResponses.setLayoutManager(linearLayoutManager);
        responsesAdapter = new ResponsesAdapter(responses, this);
        savedResponses.setAdapter(responsesAdapter);
    }
    private void getSavedResponses() {
        // String answers = db.getAllAnswers().toString();
        // Log.d(TAG,answers);
        try {

            // get saved responses ids
            HashMap<String, String> savedIDs = db.getAllAnswers();
            // Object sObj = savedIDs.get("saved");
            JSONArray sArr = new JSONArray(savedIDs.get("saved"));
            // Log.d("sArr",String.valueOf(sArr.length()));
            JSONObject savedCount = new JSONObject();

            HashMap<String, String> surveys = db.getSurveys();
            ArrayList<String> listSurveys = new ArrayList<String>();
            Object jObj = surveys.get("surveys").toString();

            JSONArray jArray = new JSONArray(jObj.toString());

            if (jArray != null) {
                for (int i=0;i<jArray.length();i++){
                    listSurveys.add(jArray.getString(i));
                }
            }

            ArrayList <String> Count = new ArrayList<>();

            /*loop through the response & set titles for each survey*/
            for (int x = 0; x < sArr.length(); x++){
                for (int i = 0; i < listSurveys.size(); i++){
                    ResponseMethods qResponses = new ResponseMethods();
                    JSONObject jsonObject = jArray.getJSONObject(i);
                    if(jsonObject.getString("sid").equals(sArr.get(x).toString())) {
                        Log.d(TAG,jsonObject.getString("title"));
                        qResponses.setsID("" + jsonObject.getString("title"));
                        qResponses.setTitle("" + jsonObject.getString("sid"));
                        // qResponses.setHelp("" + jsonObject.getString("help"));
                        responses.add(qResponses);
                    }
                    // get saved state for each survey questions to help dynamically display saved state
                }
            }
            responsesAdapter = new ResponsesAdapter(responses, this);

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClearClick(View v, int position) {
        ResponseMethods responseMethods = responses.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View clearDialog = layoutInflater.inflate(R.layout.response_clear_dialog, null);
        builder.setView(clearDialog);
        builder.setMessage("Clear Selected Responses? The selected surveys responses will be deleted permanently");
        builder.setTitle("Delete " + responseMethods.getsID() + " responses");

        TextView dialogText = v.findViewById(R.id.dialog_text);
        //dialogText.setText(responseMethods.getsID().toString());
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ResponseMethods qResponses = responses.get(position);
                String sID = qResponses.getTitle();
                db.deleteAnswers(sID);
                Toast.makeText(getContext(),"Cleared these responses from db",Toast.LENGTH_LONG).show();
                dialog.cancel();

                responses.remove(position);
                initRecyclerView();
                responsesAdapter.notifyItemRemoved(position);
                responsesAdapter.notifyItemRangeChanged(position, responses.size());

                responsesAdapter.notifyDataSetChanged();
            }
        });
        AlertDialog dialog= builder.create();
        dialog.show();
        // Toast.makeText(getApplicationContext(),"Clearinig these responses from db",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSyncClick(View v, int position) {
        ResponseMethods qResponses = responses.get(position);
        String sID = qResponses.getTitle();

        if(isConnected()){
            /*
             * GET survey responses that had been saved to sqlite db based on the survey ID
             * */
            db.getAnswers(sID);

            Log.d("answers from db", db.getAnswers(sID).toString());
            // post and sync surveys to server
            String POST_URL = " https://www.futuristickwtadev.com/index.php/api/add_responses?iSurveyID="+sID+"&aResponseData="+db.getAnswers(sID).get("answers").toString();
            String tag_string_req = "req_questions";

            StringRequest strReq = new StringRequest(Request.Method.GET, POST_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        db.addSyncedAnswers(db.getAnswers(sID).get("answers"), sID);
                        db.deleteAnswers(sID);

                        responses.remove(position);
                        initRecyclerView();
                        responsesAdapter.notifyItemRemoved(position);
                        responsesAdapter.notifyItemRangeChanged(position, responses.size());
                        responsesAdapter.notifyDataSetChanged();
                        Log.d("submit answers res", "res" + response);
                        Toast.makeText(getContext(), "Synced Successfully",Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("q", "Q Error: " + "error endpoint");
                    Toast.makeText(getContext(),error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }){

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    //params.put("iSurveyID", sID);
                    //params.put("aResponseData", db.getAnswers(sID).get("answers").toString());
                    //params.put("iSurveyID", sID);
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            strReq.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);

            /*
             * If not connected,
             * make toast to notify the user*/
        }else if(!isConnected()){
            Toast.makeText(getContext(), "Cannot Sync Without Network Connection, check network connection and try again", Toast.LENGTH_LONG).show();
            //pDialog.setMessage("");
        }

    }
    public boolean isConnected()  {
//        Runtime runtime = Runtime.getRuntime();
//        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
//            int     exitValue = ipProcess.waitFor();
//            return (exitValue == 0);
//        }
//        catch (IOException e)          { e  .printStackTrace(); }
//        catch (InterruptedException e) { e.printStackTrace(); }
//
//        return false;
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
            return true;
        } else
            connected = false;
        return false;
    }
}