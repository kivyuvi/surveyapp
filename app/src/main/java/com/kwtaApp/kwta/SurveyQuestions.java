package com.kwtaApp.kwta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kwtaApp.kwta.helpers.FilePath;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.QuestionsAdapter;
import com.kwtaApp.kwta.ui.adapter.QuestionsMethods;
import com.kwtaApp.kwta.ui.adapter.SurveyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SurveyQuestions extends AppCompatActivity implements QuestionsAdapter.onGasListener {
    public final static String SURVEY_METHOD = "survey-method";
    private ProgressDialog pDialog;
    private SQLiteHandler db;

    // location
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    String lat;
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;

    private RecyclerView mRecyclerView;
    private int PICK_FILE_REQUEST = 1;
    private String selectedFilePath = "file";


    private ArrayList<QuestionsMethods> qQuestions = new ArrayList<>();
    private QuestionsAdapter questionsAdapter;
    private QuestionsMethods questionsMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_questions);

        SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);

        Bundle pre = getIntent().getExtras();
        Log.d("t", "pre" + pre.getString("name") + "loc" + pre.getString("location"));
        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());
        TextView surveyTitle = findViewById(R.id.textView6);
        surveyTitle.setText(survey.getTitle());


        HashMap<String, String> user = db.getUserDetails();
        // initiate dialog
        pDialog = new ProgressDialog(this);

        //get parameters
        String sID = survey.getsID();
        String sSessionKey = user.get("keyToken");

        //get questions
        getSurveyQuestions(sSessionKey, sID);

        // recycler view
        mRecyclerView = findViewById(R.id.recyclerView2);
        initRecyclerView();

        //listen for submit button
        Button submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pDialog.setMessage("Saving responses Offline");
                    // pDialog.setCancelable(false);
                    showDialog();

                    HashMap<String, String> questions = db.getQuestions(sID);
                    ArrayList<String> listQuestions = new ArrayList<String>();
                    Object jObj = questions.get("question").toString();

                    //aResponse object
                    JSONObject resObj = new JSONObject();

                    JSONArray jArray = new JSONArray(jObj.toString());
                    if (jArray != null) {
                        for (int i = 0; i < jArray.length(); i++) {
                            listQuestions.add(jArray.getString(i));
                        }
                    }

                    // loop through the response
                    for (int i = 0; i < listQuestions.size(); i++) {
                        JSONObject jsonObject = jArray.getJSONObject(i);
                        String sid = jsonObject.getString("sid");
                        String gid = jsonObject.getString("gid");
                        String qid = jsonObject.getString("qid");
                        String title=jsonObject.getString("question");
                        String objKey = sid + "X" + gid + "X" + qid+ "X"+title;

                        // get answers
                        if (jsonObject.getString("type").equals("|")) {
                            String res = encodeBase64(selectedFilePath);
                            // add keys to json object
                            resObj.put(objKey, res);
                        }
                        if (jsonObject.getString("type").equals("S")) {
                            //questionsMethods.getLocation();
                            String res = qQuestions.get(i).getEditTextValue();
                            String res1 = db.getLocation().get("location");
                            //add keys to json object
                            resObj.put(objKey, res);
                            //Toast.makeText(getApplicationContext(), "This is the Short text message", Toast.LENGTH_LONG).show();
                        }
                        if (jsonObject.getString("type").equals("U")) {
                            //Huge Text Questions
                            String rest = qQuestions.get(i).getHugeTextValue();

                            // String res = db.getLocation().get("location");
                            //add keys to json object
                            resObj.put(objKey, rest);

                            //Toast.makeText(getApplicationContext(), "This is the Short text message", Toast.LENGTH_LONG).show();
                        }
                        if (jsonObject.getString("type").equals("M")) {
                            // CheckBox cBox1, cBox2, cBox3;
                            CheckBox cBox1 = findViewById(R.id.checkBox);
                            CheckBox cBox2 = findViewById(R.id.checkBox2);
                            CheckBox cBox3 = findViewById(R.id.checkBox3);

                            if (cBox1.isChecked()) {
                                Log.d("type", "Miq");
                                // Toast.makeText(getApplicationContext(),"checked",Toast.LENGTH_LONG).show();
                                String res = "soq";// questionsMethods.getCheckboxOne() ;
                                resObj.put(objKey, res);
                            }
                            if (cBox2.isChecked()) {
                                String res = "questionsMethods.getCheckboxTwo()";
                                resObj.put(objKey, res);
                            }
                            if (cBox3.isChecked()) {
                                String res = "questionsMethods.getCheckboxThree()";
                                resObj.put(objKey, res);
                            }

                        }
                        if (jsonObject.getString("type").equals("5")) {
                            RadioButton r1, r2, r3, r4, r5, r6;
                            r1 = findViewById(R.id.radia_id1);
                            r2 = findViewById(R.id.radia_id2);
                            r3 = findViewById(R.id.radia_id3);
                            r4 = findViewById(R.id.radia_id4);
                            r5 = findViewById(R.id.radia_id5);
                            // Boolean isChecked = r1.isChecked();
                            r1.setChecked(true);

                            String rest = "5";
                            resObj.put(objKey, rest);

                            if (r1.isChecked()) {
                                String res = "1";
                                resObj.put(objKey, res);
                            } else if (r2.isChecked()) {
                                String res = "2";
                                resObj.put(objKey, res);
                            } else if (r3.isChecked()) {
                                String res = "3";
                                resObj.put(objKey, res);
                            } else if (r4.isChecked()) {
                                String res = "4";
                                resObj.put(objKey, res);
                            } else if (r5.isChecked()) {
                                String res = "5";
                                resObj.put(objKey, res);
                            }
                        }

                        if (jsonObject.getString("type").equals("G")) {
                            RadioButton r1, r2, r3;
                            r1 = findViewById(R.id.gender_id1);
                            r2 = findViewById(R.id.gender_id2);
                            r3 = findViewById(R.id.gender_id3);
                            Boolean isChecked = r1.isChecked();
                            Log.d("type", isChecked.toString());

                            String rest = "M";
                            resObj.put(objKey, rest);

                            if (r1.isChecked()) {
                                String res = "M";
                                resObj.put(objKey, res);
                            } else if (r2.isChecked()) {
                                String res = "F";
                                resObj.put(objKey, res);
                            } else if (r3.isChecked()) {
                                String res = "no answer";
                                resObj.put(objKey, res);
                            }
                        }
                        if (jsonObject.getString("type").equals("Y")) {
                            RadioButton r1, r2, r3;
                            r1 = findViewById(R.id.yes_id1);
                            r2 = findViewById(R.id.no_id2);
                            r3 = findViewById(R.id.no_answer_id2);

                            String rest = "Y";
                            resObj.put(objKey, rest);

                            if (r1.isChecked()) {
                                String res = "Y";
                                resObj.put(objKey, res);
                            } else if (r2.isChecked()) {
                                String res = "N";
                                resObj.put(objKey, res);
                            } else if (r3.isChecked()) {
                                String res = "no answer";
                                resObj.put(objKey, res);
                            }
                        }
                        //arr numbers
                        if (jsonObject.getString("type").equals(":")) {
                            // String allArrayQuestions = questionsMethods.getWholeArr();
                            // JSONObject  obj = new JSONObject(allArrayQuestions);
                            JSONObject obj1 = new JSONObject(jsonObject.getString("1"));
                            JSONObject obj2 = new JSONObject(jsonObject.getString("2"));
                            JSONObject obj3 = new JSONObject(jsonObject.getString("3"));

                            // JSONArray jsonArray = new JSONArray(allArrayQuestions);
                            EditText e1 = findViewById(R.id.array_num_value);
                            EditText e2 = findViewById(R.id.array_num_value2);
                            EditText e3 = findViewById(R.id.array_num_value3);

                            String res1 = e1.getText().toString();
                            resObj.put(objKey + obj1.getString("title") + "_" + obj1.getString("title"), String.valueOf(e1.getText()));
                            resObj.put(objKey + obj2.getString("title") + "_" + obj1.getString("title"), e2.getText());
                            resObj.put(objKey + obj3.getString("title") + "_" + obj1.getString("title"), e3.getText());
                        }
                        //arr numbers
                        if (jsonObject.getString("type").equals(";")) {
                            // String allArrayQuestions = questionsMethods.getWholeArr();
                            // JSONObject  obj = new JSONObject(allArrayQuestions);
                            JSONObject obj1 = new JSONObject(jsonObject.getString("0"));
                            JSONObject obj2 = new JSONObject(jsonObject.getString("1"));
                            JSONObject obj3 = new JSONObject(jsonObject.getString("2"));

                            // JSONArray jsonArray = new JSONArray(allArrayQuestions);
                            EditText e1 = findViewById(R.id.array_text_value);
                            EditText e2 = findViewById(R.id.array_text_value2);
                            EditText e3 = findViewById(R.id.array_text_value3);

                            String res1 = e1.getText().toString();
                            resObj.put(objKey + obj1.getString("title") + "_" + obj1.getString("title"), String.valueOf(e1.getText()));
                            resObj.put(objKey + obj2.getString("title") + "_" + obj1.getString("title"), e2.getText());
                            resObj.put(objKey + obj3.getString("title") + "_" + obj1.getString("title"), e3.getText());
                        }
                        //else {
                        //   String res = qQuestions.get(i).getEditTextValue();
                        // add keys to json object
                        //    resObj.put(objKey, res);
                        //}
                    }
                    resObj.put("token", sSessionKey);
                    Log.d("all text", resObj.toString());
                    // add questions and answers to the db
                    db.deleteAnswers(sID);
                    db.addAnswers(resObj.toString(), sID);
                    hideDialog();
                    finish();
                    Toast.makeText(getApplicationContext(), "Surveys saved offline successfully", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        questionsAdapter = new QuestionsAdapter(qQuestions, this);
        mRecyclerView.setAdapter(questionsAdapter);

    }

    private boolean isNetworkAvailable() {

        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
            return true;
        } else
            connected = false;
        return false;

    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void getAnswerOptionsL() {

    }


    public void getSurveyQuestions(String sSessionKey, String sID) {
        // Tag used to cancel the request
        String tag_string_req = "req_questions";

        //String HTML_URL = "http://www.futuristickwtadev.com/index.php/api/list_questions/?iSurveyID=383811";

        try {
            //get survey methods and get active survey id
            SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);
            String survey_id = survey.getsID();
            //get questions based on the survey id
            HashMap<String, String> questions = db.getQuestions(survey_id);

            Log.d("dis", "qi" + questions.get("question"));
            ArrayList<String> listQuestions = new ArrayList<String>();
            Object jObj = questions.get("question").toString();

            JSONArray jArray = new JSONArray(jObj.toString());
            if (jArray != null) {
                for (int i = 0; i < jArray.length(); i++) {
                    listQuestions.add(jArray.getString(i));
                    //shareItem(jArray.getString(i));
                }
            }

            // loop through the response
            for (int i = 0; i < listQuestions.size(); i++) {
                questionsMethods = new QuestionsMethods();
                JSONObject jsonObject = jArray.getJSONObject(i);
                String myJ = jsonObject.toString();
                // shareItem(myJ);


                if (jsonObject.getString("type").equals("L")) {
                    questionsMethods.setsQuestion(""+jsonObject.getString("title"));
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> answers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {

                                //int value=0;

                                JSONObject object = (JSONObject) jsonObject.get(key);
                                String answer = object.get("answer").toString();
                                answers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }
                    }

                    questionsMethods.setAnswers(answers);
                }





                if (jsonObject.getString("type").equals("O")) {
                    questionsMethods.setsQuestion(""+jsonObject.getString("title"));
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> answers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {

                                //int value=0;

                                JSONObject object = (JSONObject) jsonObject.get(key);
                                String answer = object.get("answer").toString();
                                answers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }
                    }

                    questionsMethods.setTypeOlist(answers);
                }
                if (jsonObject.getString("type").equals(":")) {
                    questionsMethods.setsQuestion(""+jsonObject.getString("title"));
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> answers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {

                                //int value=0;

                                JSONObject object = (JSONObject) jsonObject.get(key);
                                String answer = object.get("answer").toString();
                                answers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }
                    }

                    questionsMethods.setTypecolonlist(answers);
                }
                if (jsonObject.getString("type").equals(":;")) {
                    questionsMethods.setsQuestion(""+jsonObject.getString("title"));
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> answers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {

                                //int value=0;

                                JSONObject object = (JSONObject) jsonObject.get(key);
                                String answer = object.get("answer").toString();
                                answers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }
                    }

                    questionsMethods.setTypesemiandcolonlist(answers);
                }
                if (jsonObject.getString("type").equals("!")){
                    Iterator<String> keys=jsonObject.keys();
                    ArrayList<String> listanswers=new ArrayList<>();
                    while(keys.hasNext()){
                        String key=keys.next();
                        if(jsonObject.get(key) instanceof  JSONObject){
                            try{
                                JSONObject object=(JSONObject) jsonObject.get(key);
                                String answer=object.get("answer").toString();
                                listanswers.add(answer);

                            }catch(Exception ex){

                            }
                        }
                    }
                    questionsMethods.setTypelist(listanswers);

                }
                if (jsonObject.getString("type").equals("A")){
                    Iterator<String> keys=jsonObject.keys();
                    ArrayList<String> listanswers=new ArrayList<>();
                    while(keys.hasNext()){
                        String key=keys.next();
                        if(jsonObject.get(key) instanceof  JSONObject){
                            try{
                                JSONObject object=(JSONObject) jsonObject.get(key);
                                String answer=object.get("answer").toString();
                                listanswers.add(answer);

                            }catch(Exception ex){

                            }
                        }
                    }
                    questionsMethods.setTypeAanswers(listanswers);

                }
                if (jsonObject.getString("type").equals("M")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> checkboxanswers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());
                                String answer = object.get("question").toString();
                                //shareItem(answer);
                                checkboxanswers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                    questionsMethods.setcboxAnswers(checkboxanswers);



                }
                if (jsonObject.getString("type").equals("P")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> checkboxanswers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());
                                String answer = object.get("question").toString();
                                //shareItem(answer);
                                checkboxanswers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                    questionsMethods.setPboxanswers(checkboxanswers);



                }
                if (jsonObject.getString("type").equals("1")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> type1answers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());

                                String answer = object.get("answer").toString();
                               // shareItem(answer);
                                type1answers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                    questionsMethods.setType1answers(type1answers);
                }
                if (jsonObject.getString("type").equals("C")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> typeCanswers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());

                                String answer = object.get("question").toString();
                                //shareItem(answer);
                                typeCanswers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                    questionsMethods.setTypeCanswers(typeCanswers);
                }

                if (jsonObject.getString("type").equals(";")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> typeCanswers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());

                                String answer = object.get("question").toString();
                                //shareItem(answer);
                                typeCanswers.add(answer);
                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                    questionsMethods.setTypeAnswers(typeCanswers);
                }
                if (jsonObject.getString("type").equals("E")) {
                    Iterator<String> keys = jsonObject.keys();

                    ArrayList<String> typeEanswers = new ArrayList<>();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                //shareItem(object.toString());

                                String answer = object.get("question").toString();
                                //shareItem(answer);
                                typeEanswers.add(answer);
                            } catch (Exception ex) {
                                ex.getMessage();

                            }
                        }


                    }
                    questionsMethods.setTypeEanswers(typeEanswers);
                }


                questionsMethods.setTitle("" + jsonObject.getString("title"));
                questionsMethods.setsQuestion("" + jsonObject.getString("question"));
                questionsMethods.setHelp("" + jsonObject.getString("help"));
                questionsMethods.setType("" + jsonObject.getString("type"));
                questionsMethods.setLocation("GPS coordinates :" + db.getLocation().get("location"));
                /*
                 * objects for checkbox*/

                if (jsonObject.getString("type").equals("M")) {
                    JSONObject checks = new JSONObject();
                    // String checksString = checks.toString();
//                        checks.put("one", jsonObject.getString("0"));
//                        checks.put("two", jsonObject.getString("1"));
//                        checks.put("three", jsonObject.getString("2"));
//                        questionsMethods.setCheckbox(checks);
                    CommonFunctions commonFunctions = new CommonFunctions();
                } else if (jsonObject.getString("type").equals(":")) {
                    JSONObject arrayNum = new JSONObject();
                    arrayNum.put("one", jsonObject.getString("1"));
                    arrayNum.put("two", jsonObject.getString("2"));
                    arrayNum.put("three", jsonObject.getString("3"));
                    questionsMethods.setArrayNumbers(arrayNum);
                }
//                    else if(jsonObject.getString("type").equals(";")){
//                        JSONObject arrayText = new JSONObject();
//                        arrayText.put("one", jsonObject.getString("0"));
//                        arrayText.put("two", jsonObject.getString("1"));
//                        arrayText.put("three", jsonObject.getString("2"));
//                        questionsMethods.setArrayText(arrayText);
//                    }else if(jsonObject.getString("type") == "|"){
//                        questionsMethods.setFileName(selectedFilePath.toString());
//                    }else if(jsonObject.getString("type").equals("F")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectf=new JSONObject(abc);
//                        String namebox=jsonObjectf.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//
//                    }else if(jsonObject.getString("type").equals("C")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectc=new JSONObject(abc);
//                        String namebox=jsonObjectc.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals("E")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjecte=new JSONObject(abc);
//                        String namebox=jsonObjecte.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals("A")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjecta=new JSONObject(abc);
//                        String namebox=jsonObjecta.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals("B")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectb=new JSONObject(abc);
//                        String namebox=jsonObjectb.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals("!")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectdd=new JSONObject(abc);
//                        String namebox=jsonObjectdd.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals("F")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectf=new JSONObject(abc);
//                        String namebox=jsonObjectf.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }else if(jsonObject.getString("type").equals(";")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectpp=new JSONObject(abc);
//                        String namebox=jsonObjectpp.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }
//                    else if(jsonObject.getString("type").equals("U")){
//                        CommonFunctions commonFunctions = new CommonFunctions();
//                        String abc = commonFunctions.getChildObject(jsonObject.toString());
//                        JSONObject jsonObjectu=new JSONObject(abc);
//                        String namebox=jsonObjectu.getString("answer");
//                        Toast.makeText(getApplicationContext(),namebox,Toast.LENGTH_LONG).show();
//                    }
                else if (jsonObject.get("type") == "L") {
                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {
                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);
                                String answer = object.get("answer").toString();

                                //Toast.makeText(SurveyQuestions.this, "This is it" + answer + "/n", Toast.LENGTH_LONG).show();
                                Log.d("SurveyQuestions Eerror", answer);
                                //txtoption.setText(answer);
                                //btn1.append("1");
                                shareItem(answer);
                                break;


                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }
                } else if (jsonObject.getString("type") == "Z") {
                    for (int l = 0; l < listQuestions.size(); l++) {
                        questionsMethods = new QuestionsMethods();
                        JSONObject jsonObjectl = jArray.getJSONObject(l);
                        String myJl = jsonObjectl.toString();
                        //shareItem(myJl);


                        CommonFunctions commonFunctions = new CommonFunctions();
                        //String abc = commonFunctions.getChildObject(jsonObject.toString());
                        String abc = commonFunctions.getChildObject(myJl);
                        //shareItem(abc);
//                    JSONObject jsonObject11 = new JSONObject(abc);
//                    JSONArray jsonArray = new JSONArray(jsonObject11);
//                    String namecbox = jsonObject11.getString("answer");
//                    Toast.makeText(getApplicationContext(), namecbox, Toast.LENGTH_LONG).show();
                        //shareItem(namecbox);

                    }

//                            CommonFunctions commonFunctions = new CommonFunctions();
//                            String abc = commonFunctions.getChildObject(jsonObject.toString());
//                            //String abc = commonFunctions.getChildObject(myJ);
//                             shareItem(abc);
//                            JSONObject jsonObject11 = new JSONObject(abc);
//
//                            JSONArray jsonArray = new JSONArray(jsonObject11);
//                            String namecbox = jsonObject11.getString("answer");
//                            Toast.makeText(getApplicationContext(), namecbox, Toast.LENGTH_LONG).show();
//                            shareItem(namecbox);
                    //}

//


                }

                qQuestions.add(questionsMethods);
            }
            questionsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void shareItem(String toshare) {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, toshare);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }


    private String readFile(String path) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }

    @Override
    public void onGasClick(View v, int position) {
        Toast.makeText(getApplicationContext(), "Connect to network to Sync", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCheckClick(View v, int position) {
        Log.d("type", "Miq");
        CheckBox cBox1 = findViewById(R.id.checkBox);
        // checkBox.setChecked(true);
        cBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cBox1.setChecked(true);
            }
        });
    }

    @Override
    public void onSyncClick(View v, int position) {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent, "Choose File to Upload.."), PICK_FILE_REQUEST);
        Log.d("v", String.valueOf(position));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FILE_REQUEST) {
                if (data == null) {
                    //no data present
                    return;
                }
                TextView fileName = findViewById(R.id.file_name);
                Uri selectedFileUri = data.getData();
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
                    selectedFilePath = FilePath.getPath(this, selectedFileUri);
                    String base64Encoded = encodeBase64(selectedFilePath);
                    Log.i("TAG", "Selected File Path:" + base64Encoded);
                    QuestionsMethods questionsMethods = new QuestionsMethods();
                    questionsMethods.setFileName(base64Encoded);

                    if (selectedFilePath != null && !selectedFilePath.equals("")) {
                        // fileName.setText(selectedFilePath.toString());
                    } else {
                        Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Permision", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }
            }
        }
    }

    private static String encodeBase64(String path) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return imageString;
    }


}
