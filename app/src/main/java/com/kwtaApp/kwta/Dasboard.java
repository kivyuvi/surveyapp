package com.kwtaApp.kwta;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.helpers.SessionManager;
import com.kwtaApp.kwta.ui.views.Login;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;

public class Dasboard extends AppCompatActivity {
    private SessionManager session;
    private SQLiteHandler db;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dasboard);

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        Button btn1 = (Button)findViewById(R.id.dashboard_surveys);
        Button btn2 = (Button)findViewById(R.id.dashboard_responses);
        Button btn3 = (Button)findViewById(R.id.dashboard_cache);
        Button btn4 = (Button)findViewById(R.id.dashboard_sync);
        Button btn5 = (Button)findViewById(R.id.dashboard_logout);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast.makeText(getApplicationContext(), "Attempting to load Surveys...", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Dasboard.this, MainActivity.class);
                     startActivity(intent);

                } catch (Exception ex)
                {
                    Toast.makeText(getApplicationContext(), "Error loading MainActivity.class\n"+ex.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resClick();

            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //db.deleteSurveys();
                //db.deleteAllQuestions();
                // db.deleteAllAnswers();
                //db.deleteLocation();
                new Handler().postDelayed(new Runnable() {
                    // Using handler with postDelayed called runnable run method
                    @Override
                    public void run() {
                        clearCache();
                        hideDialog();



                    }
                }, 3*1000);
                pDialog.setMessage("Clearing Cached Data ...");
                showDialog();
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resClick();
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                logoutUser();
            }
        });
    }

    private void resClick() {
        if(getResposes()){
            Intent intent = new Intent(Dasboard.this, AllSaveResponses.class);
            startActivity(intent);
        }else if(!getResposes()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View clearDialog = layoutInflater.inflate(R.layout.response_clear_dialog, null);
            builder.setView(clearDialog);
            builder.setMessage("You have no responses saved offline, Proceed to surveys?");
            builder.setTitle(" No Saved Responses");

            TextView dialogText = findViewById(R.id.dialog_text);
            //dialogText.setText(responseMethods.getsID().toString());
            builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Dasboard.this, MainActivity.class);
                    startActivity(intent);
                    dialog.cancel();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //ResponseMethods qResponses = survey.get(position);
                    dialog.cancel();
                }
            });
            AlertDialog dialog= builder.create();
            dialog.show();

        }
    }
    private void clearCache() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Dasboard.this);
        builder.setMessage("Do you want to Clear Cache?");
        builder.setTitle("Clear Cache");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Cache Cleared", Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "No Cache Cleared", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
      //  Create the Dialog Box
        AlertDialog.Builder builder=new AlertDialog.Builder(Dasboard.this);
        // Set the message show for the Alert time
        builder.setMessage("Do you want to Logout??");
        //Set the title
        builder.setTitle(" Logout Alert!!");
        // Set Cancelable false
        // for when the user clicks on the outside
        // the Dialog Box then it will remain show
        builder.setCancelable(false);
        // Set the positive button with yes name
        // OnClickListener method is use of
        // DialogInterface interface.
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Launching the login activity
                Intent intent = new Intent(Dasboard.this, Login.class);
                startActivity(intent);
                finish();
            }
        });
        // Set the Negative button with No name
        // OnClickListener method is use
        // of DialogInterface interface.
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // If user click no
                // then dialog box is canceled.
                dialogInterface.cancel();
            }
        });
        // Create the Alert dialog
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
    public boolean getResposes(){
        HashMap<String, String> allResponses = db.getAllAnswers();
        if(allResponses.get("saved") != null){
            return true;
        }
        return false;
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}