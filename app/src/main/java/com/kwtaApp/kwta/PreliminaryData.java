package com.kwtaApp.kwta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.SurveyMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PreliminaryData extends AppCompatActivity {
    public final static String SURVEY_METHOD = "survey-method";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView imageView;
    private String image;
    private SQLiteHandler db;
    //Location Fields
    FusedLocationProviderClient fusedLocationProviderClient;
    TextView txtlocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preliminary_data);

        PreliminaryData.this.setTitle("Fill the form to continue ...");
        db = new SQLiteHandler(getApplicationContext());
        imageView = findViewById(R.id.photo_imageview);

        //set survey title
        SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);
        TextView title = findViewById(R.id.pre_survey_title);
        title.setText("Preliminary Data for:  " + survey.getTitle());

        HashMap<String, String> user = db.getUserDetails();
        EditText name = (EditText) findViewById(R.id.name_editText);

        name.setText(user.get("name"));


        // Get Location Details
        String loc = getLocation();

        txtlocation = findViewById(R.id.location_editText);


        Button photoBtn = findViewById(R.id.photo_btn);

        photoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });


        Button preButton = findViewById(R.id.submit_pre);
        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageView.getDrawable() == null) {
                    Toast.makeText(PreliminaryData.this, "Please take your picture before proceeding", Toast.LENGTH_LONG).show();
                }
                else {


                    String sID = survey.getsID();
                    HashMap<String, String> getQuestions = db.getQuestions(sID);


                    if (getQuestions.size() > 0) {
                        /*
                         * Create objects for the survey and preliminary data*/
                        JSONObject preliminaryData = new JSONObject();
                        JSONObject allData = new JSONObject();
                        JSONObject JOB = new JSONObject();
                        try {
                            if (isConnected()) {
                                Intent intent = new Intent(PreliminaryData.this, QuestionsActivity.class);
                                intent.putExtra(SURVEY_METHOD, survey);
                                startActivity(intent);
                                finish();

                            } else {
                                EditText name = (EditText) findViewById(R.id.name_editText);
                                TextView loc1=(TextView) findViewById(R.id.location_editText) ;
                                // GET text for username and image
                                String username = name.getText().toString();
                                String loc = getLocation();
                                /* add data to the preliminary obj
                                 * add survey and preliminary data to a single object
                                 * pass to the next intent */
                                preliminaryData.put("username", username);
                                preliminaryData.put("location", loc);
                                preliminaryData.put("image", image);

                                allData.put("survey", survey);
                                allData.put("preliminaryData", preliminaryData);

                                Intent intent = new Intent(PreliminaryData.this, SurveyQuestions.class);
                                intent.putExtra("name", username);
                                intent.putExtra("location", loc);
                                intent.putExtra("image", image);

                                intent.putExtra(SURVEY_METHOD, survey);
                                startActivity(intent);
                                finish();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "These Survey Questions have not been saved offline", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //imageView = findViewById(R.id.photo_imageview);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] bb = bos.toByteArray();
            image = Base64.encodeToString(bb, Base64.DEFAULT);

            Log.d("data", image);

        }
    }

    public boolean isConnected() {
//        Runtime runtime = Runtime.getRuntime();
//        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
//            int     exitValue = ipProcess.waitFor();
//            return (exitValue == 0);
//        }
//        catch (IOException e)          { e  .printStackTrace(); }
//        catch (InterruptedException e) { e.printStackTrace(); }
//
//        return false;
        //Connectivity Code
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
            return true;
        } else
            connected = false;
        return false;

    }

    //Location Code
    private String getLocation() {
        TextView location=(TextView)findViewById(R.id.location_editText) ;
        //Initialize FusedLocationProviderClient
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        //Check Permission
        if (ActivityCompat.checkSelfPermission(PreliminaryData.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //When Permission is Granted
            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(Task<Location> task) {
                    //Initialize Location
                    Location location=task.getResult();
                    if(location!=null){
                        try {

                            //Initialize GeoCoder
                            Geocoder geocoder=new Geocoder(PreliminaryData.this,
                                    Locale.getDefault());
                            //Initialize Addresses list
                            List<Address>addresses=geocoder.getFromLocation(
                                    location.getLatitude(),location.getLongitude(),1
                            );
                            //Set Locale on TextView
                          txtlocation.setText(addresses.get(0).getLocality() +","+addresses.get(0).getCountryName());


                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }else{

                        Toast.makeText(PreliminaryData.this,"Cant get your Location Details",Toast.LENGTH_LONG).show();
                    }


                }
            });

        } else {
            //When Permission is Denied
            ActivityCompat.requestPermissions(PreliminaryData.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);

        }
        return null;
    }




 }