package com.kwtaApp.kwta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.SurveyMethods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class QuestionsActivity extends AppCompatActivity {
    public final static String SURVEY_METHOD = "survey-method";
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    private String filename;
    private  String filepath = "MyFileStorage";
    File myExternalFile;
    String myData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);
        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // initiate dialog
        pDialog = new ProgressDialog(this);

        TextView textView = findViewById(R.id.textView3);
        textView.setText("Loading Survey...");

        String sID = survey.getsID();
        Log.d("chweez","extra" + survey.getsID());

        HashMap<String, String> user = db.getUserDetails();

        String sSessionKey = user.get("keyToken");
        Log.d("session", "key" + sSessionKey);

        getHtml(sSessionKey, sID);
        // webView = new WebView(webView);

        // readFile();

        //get webview
        filename = survey.getsID();
//

        WebView webView = findViewById(R.id.web_view);
            // loading ONLINE
            Log.d("session", "WEBVIEW");
            webView.getSettings().setAllowContentAccess(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            webView.setWebViewClient(new WebViewClient());

        if(!isNetworkAvailable()) {
            Log.d("session", "WEBVIEW OFFLINE ");
            //webView.getSettings().setAllowFileAccess(true);
            // webView.getSettings().setAllowContentAccess(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            webView.loadUrl("https://www.futuristickwtadev.com/index.php/"+sID+"/?sSessionKey="+sSessionKey);
             webView.loadUrl("file:///storage/emulated/0/Android/data/com.kwtaApp.kwta/files/MyFileStorage/"+filename+".html");
        }
        webView.loadUrl("https://www.futuristickwtadev.com/index.php/"+sID+"/?sSessionKey="+sSessionKey);
    }



    private boolean isNetworkAvailable() {
//        Runtime runtime = Runtime.getRuntime();
//        try {
//            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
//            int     exitValue = ipProcess.waitFor();
//            return (exitValue == 0);
//        }
//        catch (IOException e)          { e.printStackTrace(); }
//        catch (InterruptedException e) { e.printStackTrace(); }
//
//        return false;
        //Connectivity Code
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
            return true;
        } else
            connected = false;
        return false;
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    public void getHtml(String sSessionKey, String sID) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";


        // String HTML_URL = "https://www.futuristickwtadev.com/index.php/"+sID+"/?sSessionKey="+sSessionKey;
        String HTML_URL = "https://www.futuristickwtadev.com/index.php/api/list_questions/?iSurveyID="+sID;
        if(isNetworkAvailable()){
            StringRequest strReq = new StringRequest(Request.Method.GET,
                    HTML_URL, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    Log.d("q", " ResponseE: " + response.toString());
                    String fileContents = response;
                    SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);
                    filename = survey.getsID()+".html";

                    try {
                        myExternalFile = new File(getExternalFilesDir(filepath), filename);
                        Log.d("q", " Response: " + myExternalFile);
                        FileOutputStream fos = new FileOutputStream(myExternalFile);
                        fos.write(fileContents.toString().getBytes());
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("q", "Q Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to login url
                    Map<String, String> params = new HashMap<String, String>();
                    // params.put("sSessionKey", sSessionKey);
                    params.put("iSurveyID", sID);

                    return params;
                }
            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }else if(!isNetworkAvailable()){
            // readFile();

        }


    }

    private String readFile(String path) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(path));
        try
        {
            String line = null;
            while ((line = br.readLine())!=null)
            {
                sb.append(line);
            }
        }
        finally
        {
            br.close();
        }
        return sb.toString();
    }


    //on pause
}