package com.kwtaApp.kwta.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;


    // Database Name
    private static final String DATABASE_NAME = "kwtadb";

    // Login table name
    private static final String TABLE_USER = "user";
    //Survey Table
    private static final String TABLE_SURVEY = "surveys";
    //questions Table
    private static final String TABLE_QUESTIONS = "questions";
    //answers table
    private static final String TABLE_ANSWERS = "answers";
    private static final String TABLE_SYNCED_ANSWERS = "synced_answers";
    //location table
    private  static  final String TABLE_LOCATION = "location";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_SESSION_KEY = "keyToken";

    // Survey Table Columns names
    private static final String SURVEYS = "surveys";

    // Questions Table Columns names
    private static final String QUESTIONS = "questions";
    private static final String KEY_SID = "sid";

    // answers Table Columns names
    private static final String ANSWERS = "answers";

    // answers Table Columns names
    private static final String SYNCED_ANSWERS = "synced_answers";

    // location table columns
    private  static  final  String LOCATION = "location";

    //status column
    private  static  final  String STATUS = "status";

    public SQLiteHandler(Context context)
    {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

/*        //users
    String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " TEXT,"
               + KEY_SESSION_KEY + " TEXT" + ")";
       db.execSQL(CREATE_LOGIN_TABLE);


        //surveys
      String CREATE_SURVEY_TABLE = "CREATE TABLE " + TABLE_SURVEY + "("
                + SURVEYS + " TEXT"+ ")";
      db.execSQL(CREATE_SURVEY_TABLE);


        //questions
       String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + QUESTIONS + " TEXT," + KEY_SID + " TEXT" + ")";
       db.execSQL(CREATE_QUESTIONS_TABLE);

       //Answers
       String CREATE_ANSWERS_TABLE = "CREATE TABLE " + TABLE_ANSWERS + "("
              + ANSWERS + " TEXT," + KEY_SID + " TEXT" + ")";
        db.execSQL(CREATE_ANSWERS_TABLE);
       Log.d(TAG, "Database tables created");

        //Synced Answers
        String CREATE_SYNCED_ANSWERS_TABLE = "CREATE TABLE " + TABLE_SYNCED_ANSWERS + "(" + ANSWERS + " TEXT," + KEY_SID + " TEXT" + ")";
       db.execSQL(CREATE_SYNCED_ANSWERS_TABLE);
      Log.d(TAG, "Database tables created");

        //location
       String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "("
               + LOCATION + " TEXT" + ")";
        db.execSQL(CREATE_LOCATION_TABLE);
        Log.d(TAG, "Database tables created");

        */

// MY CODE BEGINS
        //users
     String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " TEXT,"
               + KEY_SESSION_KEY + " TEXT" + ")";
       db.execSQL(CREATE_LOGIN_TABLE);


        //surveys
       String CREATE_SURVEY_TABLE = "CREATE TABLE " + TABLE_SURVEY + "("
                + SURVEYS + " TEXT "+ " NOT NULL)";
       db.execSQL(CREATE_SURVEY_TABLE);

        //questions
       String CREATE_QUESTIONS_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + QUESTIONS + " TEXT," + KEY_SID + " TEXT" + ")";
       db.execSQL(CREATE_QUESTIONS_TABLE);

       //Answers
       String CREATE_ANSWERS_TABLE = "CREATE TABLE " + TABLE_ANSWERS + "("
              + ANSWERS + " TEXT," + KEY_SID + " TEXT" + ")";
        db.execSQL(CREATE_ANSWERS_TABLE);
       Log.d(TAG, "Database tables created");

        //Synced Answers
        String CREATE_SYNCED_ANSWERS_TABLE = "CREATE TABLE " + TABLE_SYNCED_ANSWERS + "(" + ANSWERS + " TEXT," + KEY_SID + " TEXT" + ")";
       db.execSQL(CREATE_SYNCED_ANSWERS_TABLE);
      Log.d(TAG, "Database tables created");

        //location
       String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "("
               + LOCATION + " TEXT" + ")";
        db.execSQL(CREATE_LOCATION_TABLE);
        Log.d(TAG, "Database tables created");
// MY CODE ENDS



    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String name, String email, String uid, String KeyToken) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid); // Email
        values.put(KEY_SESSION_KEY, KeyToken); // Created At

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public boolean addSurvey(String surveys) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SURVEYS, surveys); //survey

        // Inserting Row
        long id = db.insert(TABLE_SURVEY, null, values);
        if(id==-1){
            return false;

        }
        db.close(); // Closing database connection
        Log.d(TAG, "New survey inserted into sqlite: " + id);

        return  true;

    }

    //ggggggggggggggggggg
//    public void updateSyncStatus(String sid) {
//        String[] param = {sid};
//        // calling a method to get writable database.
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        // on below line we are passing all values
//        // along with its key and value pair.
//        values.put(STATUS, "1");
//        // on below line we are calling a update method to update our database and passing our values.
//        // and we are comparing it with name of our course which is stored in original name variable.
//        db.update(TABLE_QUESTIONS, values, "sid=?", param);
//        db.close();
//    }

    public void addQuestions(String questions,String sid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTIONS, questions);
        values.put(KEY_SID, sid);
        // Inserting Row
        long id = db.insert(TABLE_QUESTIONS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New question inserted into sqlite: " + values);
    }

    public void addAnswers(String answers,String sid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ANSWERS, answers);
        values.put(KEY_SID, sid);
        // Inserting Row
        long id = db.insert(TABLE_ANSWERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New Answers inserted into sqlite: " + values);
    }
    public void addSyncedAnswers(String answers,String sid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ANSWERS, answers);
        values.put(KEY_SID, sid);
        // Inserting Row
        long id = db.insert(TABLE_SYNCED_ANSWERS, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New Answers inserted into sqlite: " + values);
    }

    public  void addLocation(String location){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LOCATION, location);
        // Inserting Row
        long id = db.insert(TABLE_LOCATION, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New location inserted into sqlite: " + values);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("keyToken", cursor.getString(4));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    public HashMap<String, String> getSurveys() {
        HashMap<String, String> survey = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_SURVEY;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            survey.put("surveys", cursor.getString(0));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching surveys from Sqlite: " + survey.toString());

        return survey;
    }

    public HashMap<String, String> getQuestions(String sid) {
        HashMap<String, String> questions = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_QUESTIONS + " WHERE sid = '"+sid+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor!=null && cursor.getCount() > 0) {
            questions.put("question", cursor.getString(0));
            questions.put("sid", cursor.getString(1));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching question from Sqlite: " + questions.toString());

        return questions;
    }


    public HashMap<String, String> getAnswers(String sid) {
        HashMap<String, String> answers = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_ANSWERS + " WHERE sid = '"+sid+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor!=null && cursor.getCount() > 0) {
            answers.put("answers", cursor.getString(0));
            answers.put("sid", cursor.getString(1));
        }
        cursor.close();
        db.close();
        // return answers
        Log.d(TAG, "Fetching answers from Sqlite: " + answers.toString());

        return answers;
    }
    public HashMap<String, String> getSyncedAnswers() {
        HashMap<String, String> answers = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_SYNCED_ANSWERS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> list = new ArrayList();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String  id= cursor.getString(cursor.getColumnIndex("sid"));
                String answer = cursor.getString(cursor.getColumnIndex("answers"));
                list.add(id);
                cursor.moveToNext();
                //answers.put("saved",answer);
            }
            answers.put("saved", list.toString());
        }
        cursor.close();
        db.close();

        Log.d(TAG, "Fetching all Synced answers from Sqlite: "+list );

        return answers;
    }
    public HashMap<String, String> getAllAnswers() {
        HashMap<String, String> answers = new HashMap<>();
        String selectQuery = "SELECT  * FROM " + TABLE_ANSWERS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> list = new ArrayList();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String  id= cursor.getString(cursor.getColumnIndex("sid"));
                String answer = cursor.getString(cursor.getColumnIndex("answers"));
                list.add(id);
                cursor.moveToNext();
                //answers.put("saved",answer);
            }
            answers.put("saved", list.toString());
        }
        cursor.close();
        db.close();

        Log.d(TAG, "Fetching all answers from Sqlite: "+list );

        return answers;
    }

    public HashMap<String, String> getLocation() {
        HashMap<String, String> location = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            location.put("location", cursor.getString(0));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching surveys from Sqlite: " + location.toString());

        return location;
    }
    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
    public void deleteSurveys() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_SURVEY, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
    public void deleteQuestions(String sID) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All specific questions
        String s = "DELETE FROM questions WHERE sid=" + sID;
        db.execSQL(s);

        db.close();
        Log.d(TAG, "Deleted " +sID+" Questions info from sqlite");
    }
    public void deleteAllQuestions() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_QUESTIONS, null, null);
        db.close();

        Log.d(TAG, "Deleted ALL Questions  from sqlite");
    }
    public void deleteAnswers(String sID) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All specific questions
        String s = "DELETE FROM answers WHERE sid=" + sID;
        db.execSQL(s);

        db.close();

        Log.d(TAG, "Deleted " +sID+" Answers info from sqlite");
    }
    public void deleteSyncedAnswers(String sID) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All specific questions
        String s = "DELETE FROM synced_answers WHERE sid=" + sID;
        db.execSQL(s);

        db.close();

        Log.d(TAG, "Deleted " +sID+" Answers info from sqlite");
    }
    public void deleteAllAnswers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All specific questions
        db.delete(TABLE_ANSWERS, null, null);
        db.close();


        Log.d(TAG, "Deleted All  Answers info from sqlite");
    }
    public void deleteLocation() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_LOCATION, null, null);
        db.close();

        Log.d(TAG, "Deleted location info from sqlite");
    }

}
