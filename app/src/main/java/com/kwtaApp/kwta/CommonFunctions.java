package com.kwtaApp.kwta;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class CommonFunctions {


    public CommonFunctions() {

    }

    public void myAlertDialog(Context context, String Title, String Message) {
        try {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(Message);
            builder.setTitle(Title);
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //    Toast.makeText(context, "A, Toast.LENGTH_LONG).show();

                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    JSONArray array = new JSONArray();
                    // With put() you can add a value to the array.
                    // array.put(abc);
                    // Create a new instance of a JSONObject
                    JSONObject obj = new JSONObject();
                    try {
                        // Add the JSONArray to the JSONObject
                        obj.put("the_array", array);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String json = obj.toString();
                    //shareItem(json);
                    //  Toast.makeText(getApplicationContext(), "Nothing ", Toast.LENGTH_LONG).show();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception EX) {
            Toast.makeText(context, "Alert Dialog Error:\n" + EX.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public String getChildObject(String childobj) {
        try {
            String myStr = childobj;
            int a = myStr.indexOf("{", 1);
            //Omits the question
            int b = myStr.indexOf("{", 0);
            //1
            String substr;
            //substr = myStr.substring(a, myStr.length() - 1);
            substr=myStr.substring(a,myStr.length());
            return substr;
        } catch (Exception ex) {
            return null;

        }


    }

    public String FormatTypes(String answeroptions) {

        try {
            String text = "geeks1for2geeks3";

            // Specifies the string pattern which is to be searched
            String delimiter =  "\\d";
            Pattern pattern = Pattern.compile(delimiter,
                    Pattern.CASE_INSENSITIVE);

            // Used to perform case insensitive search of the string
            String[] result = pattern.split(text);

            for ( String temp: result){

                return(temp);
            }


        } catch (Exception ex) {
        ex.printStackTrace();

        }

        return answeroptions;
    }

//public void TypeLAnswerOptions(){
//    //if (myJ.contains("Which Programming Language do you prefer?")) {
//    if (jsonObject.getString("type").equals("L")) {
//
//        Iterator<String> keys = jsonObject.keys();
//
//        while (keys.hasNext()) {
//            String key = keys.next();
//            //Timber.d("Json Key: %s",key);
//            if (jsonObject.get(key) instanceof JSONObject) {
//                try {
//                    JSONObject object = (JSONObject) jsonObject.get(key);
//                    String answer = object.get("answer").toString();
//                   // TextView txtoption = (TextView) findViewById(R.id.txtRadio);
//                   // Toast.makeText(SurveyQuestions.this, "This is it" + answer + "/n", Toast.LENGTH_LONG).show();
//                    Log.d("SurveyQuestions Eerror", answer);
//                    txtoption.setText(answer);
//                    //btn1.append("1");
//                   // shareItem(answer);
//                    break;
//
//
//                } catch (Exception ex) {
//                    //shareItem(ex.getMessage());
//
//                }
//            }
//
//
//        }

//        CommonFunctions commonFunctions = new CommonFunctions();
//        String abc = commonFunctions.getChildObject(myJ);


        //shareItem(abc);
        //Gets the First Element Only
//                   JSONObject jobj1=new JSONObject(abc);
//                    shareItem(jobj1.toString());
//
        // commonFunctions.myAlertDialog(this, "wll now break down ABC", abc);
//        try {
//            String[] mystuff;
//            mystuff = commonFunctions.jsonToStringArray(abc, this);
//        } catch (Exception ex) {
//            //Toast.makeText(this, "Error checking out stuff\n\n" + ex.toString(), Toast.LENGTH_LONG).show();
//        }
//        //Array of JSon Objects
//        List<String> myArrayList = new ArrayList<String>(Arrays.asList(abc.split(",")));
//
//        //shareItem((commonFunctions.jsonToStringArray()));
//        //shareItem(myArrayList.toString());
////                            String a="a0bc{d";
        // shareItem(commonFunctions.FormatString(a));
        //shareItem(commonFunctions.FormatString(myArrayList.toString()));

//        for (int j = 0; j < myArrayList.size(); j++) {
//            //shareItem(myArrayList.get(j));
//            Log.d("The Array List output", myArrayList.get(j));
//
//        }
//    }



  public String[] jsonToStringArray(String strArray, Context ctx) {
     // Log.d("raw str", strArray + "\n");
      //this.myAlertDialog(ctx,"Raw str",strArray);

      try {
          String[] myArray = strArray.split(":");
          int i = 0;
          for (String str : myArray) {
              if (i > 0) {
                  str = str.substring(3, str.length());
                  myArray[i] = str;
              }
              myArray[i] = myArray[i] + "}";
              // OUTPUT SOMEWHERE TO SEE WHATS HAPPENING

                  Log.d("MY Array", myArray[i] + "\n");
              i++;
          }
           return myArray;
      } catch (Exception e) {
          e.printStackTrace();
          this.myAlertDialog(ctx,"Error breaking ABC",e.toString());
          Log.d("Error at JSON STRING",e.getMessage());
          return null;
         // return  null;
      }

  }




    public String FormatString(String answeroptions){


      //return  answeroptions.replace("0\":" ,"");
      String answeroptions1=
            answeroptions
                    .replace(":","")
              . replace("code","\t")
              .replace("language" ,"\t")

              .replace("sortorder" ,"")
              .replace("scale_id" ,"")
              .replace("aid" ,"")
              .replace("qid" ,"")
              .replace("id" ,"")

              .replace("assessment_value" ,"")
              .replace("AO" ,"")
              .replace("0"," " )
              .replace("1"," " )
              .replace("2" ," ")
              .replace("3"," " )
              .replace("4"," " )
              .replace("5"," " )
              .replace("6"," " )
              .replace("7"," " )
              .replace("8"," " )



                    .replace("9"," " );
//        String s = "the text=map";
//        String s1 = s.substring(s.indexOf("=")+1);
//       // String s2=answeroptions1.substring(answeroptions.indexOf(",")+62);
//        int a=answeroptions1.indexOf("assess",5);
//        String s2=answeroptions1.substring(a,answeroptions1.length());
//        s2.trim();
        return answeroptions1;

      // return  answeroptions1;





    }

}
