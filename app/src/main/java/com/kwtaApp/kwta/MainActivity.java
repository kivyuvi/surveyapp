package com.kwtaApp.kwta;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.helpers.SessionManager;
import com.kwtaApp.kwta.ui.adapter.QuestionsAdapter;
import com.kwtaApp.kwta.ui.adapter.QuestionsMethods;
import com.kwtaApp.kwta.ui.adapter.SurveyAdapter;
import com.kwtaApp.kwta.ui.adapter.SurveyMethods;
import com.kwtaApp.kwta.ui.views.Login;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements SurveyAdapter.onGasListener {
    private static final String TAG = "chweez";
    // public final static String SURVEY_METHOD = "survey-method";
    public final static String SURVEY_METHOD = "survey-method";
    private ProgressDialog pDialog;
    //db instance
    private SQLiteHandler db;
    private SessionManager session;
    public  ArrayList<String> SIDCollection;

    CommonFunctions commonFunctions = new CommonFunctions();

    //getquestion methods
    private ArrayList<QuestionsMethods> qQuestions = new ArrayList<>();
    private QuestionsAdapter questionsAdapter;

    // surveys recycler view
    private RecyclerView mRecyclerView;

    // survey methods and adapter
    private ArrayList<SurveyMethods> sSurveys = new ArrayList<>();
    private SurveyAdapter surveyAdapter;

    // location last updated time
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 100;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 100;

    private static final int REQUEST_CHECK_SETTINGS = 100;

    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private TextView sLength2;

    LocationManager locationManager;
    boolean GpsStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {

            //change toolbar title
            MainActivity.this.setTitle("KWTA SURVEYS");

            /**?/
             *set dialog
             * initialize progress dialog
             */
            pDialog = new ProgressDialog(MainActivity.this);

            // SqLite database handler
            db = new SQLiteHandler(getApplicationContext());
            // session manager - logout user if not logged in
            session = new SessionManager(getApplicationContext());
            if (!session.isLoggedIn()) {
                logoutUser();
            }

            // refresh

            //get logged in user and display the username
            HashMap<String, String> user = db.getUserDetails();
            TextView username = (TextView) findViewById(R.id.username);
            username.setText("Surveys Viewable by " + user.get("name"));

            //get permission to access user location
            getPermision();

            // initialize recycler view
            mRecyclerView = findViewById(R.id.recyclerView);
            initRecyclerView();
            // get surveys

            hideDialog();
            try {
                showDialog();
                getSurveys();
            } catch (JSONException e) {
                e.printStackTrace();
                //Toast.makeText(MainActivity.this, "This is the error" + e, Toast.LENGTH_LONG).show();
            }
            /*
             * add survey length stat*/
            HashMap<String, String> allResponses = db.getAllAnswers();
            TextView sLength3 = findViewById(R.id.saved_offline_stat);
            try {
                if (allResponses.get("saved") == null) {
                    sLength3.setText("0");
                } else {
                    JSONArray sArr = new JSONArray(allResponses.get("saved"));
                    sLength3.setText(String.valueOf(sArr.length()));

                    //Toast.makeText(getApplicationContext(),"This is the not saved stats",sArr.length()+ Toast.LENGTH_LONG).show() ;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.d("tasd", allResponses.toString());
        } catch (Exception ex) {
            // Toast.makeText(getApplicationContext(), "Mainactivity Error:\n" + ex.toString() + "\n" + "Line no: " + ex.getStackTrace()[0].toString(), Toast.LENGTH_LONG).show();
            Log.d("mainactivity_log_error", ex.toString() + "\n Line no: " + ex.getStackTrace()[0].toString());

        }



        SharedPreferences prefs = this.getSharedPreferences("SavedSurvay", Context.MODE_PRIVATE);
        String lanSettings = prefs.getString("SIDCount", null);
        Log.d(TAG, "onCreatelanSettings: " + lanSettings);
        if ((lanSettings+"").equals("null") ){
            SIDCollection = new ArrayList<String>();
        }else {
            SIDCollection = new ArrayList<String>(Arrays.asList(lanSettings.split(",")));
        }

        sLength2 = findViewById(R.id.textprev);

        String nll = ""+lanSettings;
        if(nll.equals("null")){
            sLength2.setText("0");
        }else if(!lanSettings.contains(",") && lanSettings.length() >4){
            sLength2.setText("1");
        }else {
            List<String> myList = new ArrayList<String>(Arrays.asList(lanSettings.split(",")));
            sLength2.setText(""+myList.size());
        }
        Log.d(TAG, "onCreateDDD: "+SIDCollection);



    }

    @Override
    protected void onResume() {
        super.onResume();
        HashMap<String, String> allResponses = db.getAllAnswers();
        // Toast.makeText(getApplicationContext(),"This it"+allResponses ,Toast.LENGTH_LONG).show();
        TextView sLength3 = findViewById(R.id.saved_offline_stat);
        try {
            if (allResponses.get("saved") == null) {
                sLength3.setText("0");


            } else {
                JSONArray sArr = new JSONArray(allResponses.get("saved"));
                sLength3.setText(String.valueOf(sArr.length()));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * get the side menu on create
     * define click event listeners for the menu items
     * */
//     private void EmptyCache() {
//         AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//         builder.setMessage("Do you want to Clear Cache?");
//         builder.setTitle("Clear Cache");
//         builder.setCancelable(false);
//         builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//             @Override
//             public void onClick(DialogInterface dialogInterface, int i) {
//                 Toast.makeText(getApplicationContext(), "Cache Cleared", Toast.LENGTH_LONG).show();
//
//             }
//         });
//         builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//             @Override
//             public void onClick(DialogInterface dialogInterface, int i) {
//                 Toast.makeText(getApplicationContext(), "No Cache Cleared", Toast.LENGTH_LONG).show();
//             }
//         });
//     }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dot_vert, menu);
        MenuItem mItem = menu.findItem(R.id.side_menu);
        MenuItem hItem = menu.findItem(R.id.side_help);
        MenuItem cItem = menu.findItem(R.id.clear_cache);
        MenuItem uItem = menu.findItem(R.id.update_menu);
        hItem.setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (db.getAllAnswers().get("saved") == null) {
                            Toast.makeText(getApplicationContext(), "You have" + db.getAllAnswers().size() + "responses saved offline", Toast.LENGTH_LONG).show();
                        } else {
                            Intent savedIntent = new Intent(MainActivity.this, AllSaveResponses.class);
                            // savedIntent.putExtra(user.get("name"));
                            startActivity(savedIntent);
                            return false;
                        }
                        return false;
                    }
                }
        );
        uItem.setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        try {
                            getSurveys();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                }
        );
        cItem.setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        //EmptyCache();

                        //Toast.makeText(MainActivity.this ,"Clearing Cache now",Toast.LENGTH_LONG).show();
                        db.deleteSurveys();
                        db.deleteAllQuestions();
                        // db.deleteAllAnswers();
                        db.deleteLocation();
                        //Toast.makeText(MainActivity.this,"Cache cleared" ,Toast.LENGTH_LONG).show();

                        return false;


                    }
                }
        );
        mItem.setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        logoutUser();
                        return false;
                    }
                }
        );
        return true;
    }

    //Clear Cache
    private void clearCache() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Do you want to Clear Cache?");
        builder.setTitle("Clear Cache");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "Cache Cleared", Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(), "No Cache Cleared", Toast.LENGTH_LONG).show();
            }
        });
    }

    // log out if no user is cached
    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, Login.class);
        startActivity(intent);
        finish();

    }

    // initialize the recycler view
    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        surveyAdapter = new SurveyAdapter(sSurveys, this);
        mRecyclerView.setAdapter(surveyAdapter);
    }

    /*
     * Get surveys from API endpoint
     * Check for internet connection
     * if connected, query api and update cache, then load from cache
     * if not connected, load from cache*/
    private void getSurveys() throws JSONException, NullPointerException {
        try {


            String tag_string_req = "req_sen";
            /*
             * get user cached user details
             * get sSessionKey to authenticate the request*/
            HashMap<String, String> user = db.getUserDetails();
            String sSessionKey = user.get("keyToken");

            /*Check if connected, then query the api
             * */
            if (isConnected()) {
                pDialog.setMessage("Fetching surveys ...");
                showDialog();
                Toast.makeText(getApplicationContext(), "Connection detected, Updating cache ",
                        Toast.LENGTH_LONG).show();
                StringRequest strReq = new StringRequest(Request.Method.GET,
                        AppConfig.URL_SURVEY + "?sSessionKey=" + sSessionKey,
                        new Response.Listener<String>() {
                            //New Code
//           JsonObjectRequest myRequest = new JsonObjectRequest(Request.Method.GET,
//                   AppConfig.URL_SURVEY, null,
//                   new Response.Listener<JSONObject>() {
//
//                       @Override
//                       public void onResponse(JSONObject response) {
//                           Log.d(TAG, response.toString());
//                       }
//                   }, new Response.ErrorListener() {
//
//               @Override
//               public void onErrorResponse(VolleyError error) {
//                   Log.d(TAG, "Error: " + error.getMessage());
//               }
//           });
//


                            @Override
                            public void onResponse(String response) throws NullPointerException {
                                // Log.d(TAG, "Login Response: " + response.toString());
                                /*
                                 * delete cached surveys and update cache*/
                                try {
                                    db.deleteSurveys();
                                    //Toast.makeText(getApplicationContext(), "Online response:\n"+response, Toast.LENGTH_LONG).show();

                                    db.addSurvey(response);

                                    /*get the updated surveys from cacnhe and populate the recycler view adapter*/
                                    HashMap<String, String> surveys = db.getSurveys();
                                    Toast.makeText(getApplicationContext(), "Surveys Updated Successfully", Toast.LENGTH_LONG).show();

                                    ArrayList<String> listSurveys = new ArrayList<String>();


                                    Object jObj = surveys.get("surveys").toString();
                                    //Surveys when connected

                                    String surveyObjStr =surveys.get("surveys").toString();
                                    CommonFunctions commonFunctions1=new CommonFunctions();
                                    //commonFunctions1.myAlertDialog(MainActivity.this ,"ONLINE DATA",surveyObjStr);


                                    JSONArray jArray = new JSONArray(jObj.toString());
                                    // JSONArray jsonArray = new JSONArray(Collections.reverse(Arrays.asList(jArray)));

                                    if (jArray != null) {
                                        for (int i = 0; i < jArray.length(); i++) {
                                            listSurveys.add(jArray.getString(i));
                                        }
                                    }
                                    //New Code
//                   ArrayList<String> listdata = new ArrayList<String>();
//                  JSONArray jArray = (JSONArray)jsonObject;
//                   if (jArray != null) {
//                      for (int i=0;i<jArray.length();i++){
//                          listdata.add(jArray.getString(i));
//                        }
//                    }
                                    /*
                                     * add survey length stat*/
                                    TextView sLength = findViewById(R.id.survey_length);
//                                     TextView sLength2 = findViewById(R.id.textprev);
                                    //Toast.makeText(getApplicationContext(),"Total Surveys"+listSurveys.size(),Toast.LENGTH_LONG).show();
                                    sLength.setText(String.valueOf(listSurveys.size()));

                                    /*
                                     * loop through the response
                                     * use survey methods to set adapter views*/
                                    for (int i = 0; i < listSurveys.size(); i++) {
                                        SurveyMethods survey = new SurveyMethods();
                                        JSONObject jsonObject = jArray.getJSONObject(i);
                                        survey.setTitle("" + jsonObject.getString("title"));
                                        survey.setsID("" + jsonObject.getString("sid"));
                                        survey.setsDesc("" + jsonObject.getString("description"));
                                        sSurveys.add(survey);
                                        survey.setTimestamp("Run");
                                        Log.d(TAG, "onResponsess: "+jsonObject.getString("sid"));


                                        /*get saved state for each survey questions to help dynamically display saved state*/
                                        HashMap<String, String> getQuestions = db.getQuestions(jsonObject.getString("sid"));

                                        StringBuilder QZ = new StringBuilder();

                                        for(String kk: getQuestions.keySet())
                                        {
                                            QZ.append(kk + " => "+ getQuestions.get(kk) +"\n");
                                        }



                                        if (getQuestions.size() > 0) {
                                            survey.setSavedState("saved");
                                            //sLength2.setText(String.valueOf(getQuestions.size() + 1));
                                            // Toast.makeText(getApplicationContext(),"This is a saved stat" + getQuestions.size(),Toast.LENGTH_LONG).show();
                                            //getQuestions.size();
                                        } else {
                                            survey.setSavedState("save");
                                            // Toast.makeText(getApplicationContext(),"This is a Not a saved stat",Toast.LENGTH_LONG).show();
                                        }
                                    }
                                    surveyAdapter.notifyDataSetChanged();
                                    // hideDialog();

                                } catch (JSONException e) {
                                    // JSON error
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                    logoutUser();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "fetch Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to surveys url
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sSessionKey", sSessionKey);
                        // params.put("username", username);
                        return params;
                    }
                };

                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                strReq.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(strReq);
                hideDialog();
                /*
                 * if not connected load cached surveys
                 * */
            } else if (!isConnected()) {
                pDialog.setMessage(" No internet Connection ,Fetching surveys from Cache ...");
                showDialog();


                // Toast.makeText(getApplicationContext(), "No Network connection, surveys loaded from cache ", Toast.LENGTH_LONG).show();
                HashMap<String, String> surveys = db.getSurveys();

                String MYECHECK = "[";
                for (int x = 0; x < surveys.size(); x++) {
                    MYECHECK = MYECHECK + surveys.get(x);
                }
                MYECHECK = MYECHECK + "]";
                //Toast.makeText(getApplicationContext(), " SURVEY DATA: " + MYECHECK, Toast.LENGTH_LONG).show();


                ArrayList<String> listSurveys = new ArrayList<String>();
                //NullException

                Object jObj = surveys.get("surveys").toString();

//Surveys when Offline
                String surveyObjStrOffline =surveys.get("surveys").toString();
                CommonFunctions commonFunctions=new CommonFunctions();
                // commonFunctions.myAlertDialog(MainActivity.this ,"OFFLINE DATA",surveyObjStrOffline);


                //Toast.makeText(getApplicationContext(), "* OFFLINE* Survey Object String:\n"+surveyObjStrOffline, Toast.LENGTH_LONG).show();


                JSONArray jArray = new JSONArray(jObj.toString());
                hideDialog();
                // CommonFunctions commonFunctions=new CommonFunctions();
                //commonFunctions.myAlertDialog(this ,"JARRAY DATA",jArray.toString());


                if (jArray != null) {
                    for (int i = 0; i < jArray.length(); i++) {
                        listSurveys.add(jArray.getString(i));
                    }
                }

                // add survey length stat

                TextView sLength = findViewById(R.id.survey_length);
//                 TextView sLength2 = findViewById(R.id.textprev);
                sLength.setText(String.valueOf(listSurveys.size()));
                //sLength2.setText(String.valueOf(getQuestions.size()+1));
                // sLength2.setText(String.valueOf(listSurveys.size()));
                // loop through the response & set titles for each survey
                for (int i = 0; i < listSurveys.size(); i++) {
                    SurveyMethods survey = new SurveyMethods();
                    JSONObject jsonObject = jArray.getJSONObject(i);
                    survey.setTitle("" + jsonObject.getString("title"));
                    survey.setsID("" + jsonObject.getString("sid"));
                    survey.setsDesc("" + jsonObject.getString("description"));
                    survey.setTimestamp("Run Survey");
                    survey.setSavedState("Offline");

                    sSurveys.add(survey);
                    //TextView sLength2 = findViewById(R.id.textprev);
                    //sLength2.setText(sSurveys.size());
                    // get saved state for each survey questions to help dynamically display saved state
                }
                surveyAdapter.notifyDataSetChanged();



            }
        } catch (Exception ex) {
            ex.printStackTrace();
            //Toast.makeText(getApplicationContext(), "This is the error at hand" + ex.getStackTrace(), Toast.LENGTH_LONG).show();
        }

    }

    /*Run Button Click events
     * Check for connection and redirect accordingly*/
    @Override
    public void onGasClick(View v, int position) {
        /*
            check if connected
            if connected redirect to webview
            if not connected redirect to survey questions
        */
        SurveyMethods survey = sSurveys.get(position);
        CommonFunctions commonFunctions=new CommonFunctions();
        //commonFunctions.myAlertDialog(this ,"SUrvey Questions",survey.toString());

        String sID = survey.getsID();
        HashMap<String, String> getQuestions = db.getQuestions(sID);

        if (getQuestions.size() > 0) {
            if (checkGpsStatus()) {
                Intent intent = new Intent(this, PreliminaryData.class);
                intent.putExtra(SURVEY_METHOD, survey);
                startActivity(intent);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View clearDialog = layoutInflater.inflate(R.layout.response_clear_dialog, null);
                builder.setView(clearDialog);
                builder.setMessage("Turn on GPS to Continue");
                builder.setTitle(" Enable GPS");

                TextView dialogText = findViewById(R.id.dialog_text);
                //dialogText.setText(responseMethods.getsID().toString());
                builder.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent1);
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ResponseMethods qResponses = survey.get(position);
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        } else {
            /* check if the questions are saved locally
             * if saved open the survey question class
             * else make toast message*/
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View clearDialog = layoutInflater.inflate(R.layout.response_clear_dialog, null);
            builder.setView(clearDialog);
            builder.setMessage(survey.getTitle() + " has not been saved offline, save offline?");
            builder.setTitle(" NOT SAVED OFFLINE");

            TextView dialogText = findViewById(R.id.dialog_text);
            //dialogText.setText(responseMethods.getsID().toString());
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onSaveClick(v, position);
                    dialog.cancel();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //ResponseMethods qResponses = survey.get(position);
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

            // Toast.makeText(getApplicationContext(),"These Survey Questions have not been saved offline", Toast.LENGTH_LONG).show();
        }

    }

    /*
     * Save button click.,
     * Checks for internet connection
     * Saves to cache if connected
     * makes toast if not connected*/
    @Override
    public void onSaveClick(View v, int position) {
        SurveyMethods survey = sSurveys.get(position);
        String sID = survey.getsID();
        Log.d(TAG, "onSaveClickWeee: "+sID);

        Log.d(TAG, "onSaveClick: ");
        if (!SIDCollection.contains(sID)){
            SIDCollection.add(sID);
            saveData();
        }
        Log.d(TAG, "onSaveClickdff: "+SIDCollection);


        HashMap<String, String> user = db.getUserDetails();
        String sSessionKey = user.get("keyToken");

        /*show already saved toast if questions are already saved*/
        HashMap<String, String> getQuestions = db.getQuestions(sID);
        // db.updateSyncStatus(sID);
        //Get Questions Online
//        CommonFunctions commonFunctions=new CommonFunctions();
//        commonFunctions.myAlertDialog(this,"Questions ONLINE",getQuestions.toString());
        if (getQuestions.size() > 0) {
//             TextView sLength2 = findViewById(R.id.textprev);
//             sLength2.setText(String.valueOf(getQuestions.size() + 1));
            Log.d(TAG, "onSaveClick: "+getQuestions.size() + 1);
            Toast.makeText(getApplicationContext(), "It seems the survey you are trying to save has already been saved", Toast.LENGTH_LONG).show();
        } else {
            /*check internet connection
             if connected get survey questions the api*/
            if (isConnected()) {
                pDialog.setMessage("Saving  " + survey.getTitle() + "Offline");
                showDialog();
                getAllQuestions(sID, sSessionKey);
            } else if (!isConnected()) {

                pDialog.setMessage("Make sure you are connected to the internet");
                Toast.makeText(getApplicationContext(), "Make sure you are connected to the internet", Toast.LENGTH_LONG).show();
            }
        }
    }

    //convert Arraylist to string
    private void saveData() {
        // method for saving the data in array list.
        // creating a variable for storing data in
        // shared preferences.
        SharedPreferences sharedPreferences = getSharedPreferences("SavedSurvay", MODE_PRIVATE);

        // creating a variable for editor to
        // store data in shared preferences.
        SharedPreferences.Editor editor = sharedPreferences.edit();

        // creating a new variable for gson.
        Gson gson = new Gson();

        // getting data from gson and storing it in a string.

        String json = gson.toJson(SIDCollection);

        ArrayList<String>updateCount = new ArrayList<String>(Arrays.asList(json.split(",")));

        String  newCount = ""+updateCount.size();
        sLength2.setText(newCount);

        // below line is to save data in shared
        // prefs in the form of string.
        editor.putString("SIDCount", json);

        // below line is to apply changes
        // and save data in shared prefs.
        editor.apply();

        // after saving data we are displaying a toast message.
        Toast.makeText(this, "Saved Array List to Shared preferences. ", Toast.LENGTH_SHORT).show();
    }

    private void saveData(String sid) {
        // method for saving the data in array list.
        // creating a variable for storing data in
        // shared preferences.
        SharedPreferences sharedPreferences = getSharedPreferences("SavedSurvay", MODE_PRIVATE);

        // creating a variable for editor to
        // store data in shared preferences.
        SharedPreferences.Editor editor = sharedPreferences.edit();

        // creating a new variable for gson.
        Gson gson = new Gson();

        // getting data from gson and storing it in a string.

        String json = gson.toJson(SIDCollection);

        if (!json.contains(sid)){

        }
        ArrayList<String>updateCount = new ArrayList<String>(Arrays.asList(json.split(",")));

        String  newCount = ""+updateCount.size();
        sLength2.setText(newCount);

        // below line is to save data in shared
        // prefs in the form of string.
        editor.putString("SIDCount", json);

        // below line is to apply changes
        // and save data in shared prefs.
        editor.apply();

        // after saving data we are displaying a toast message.
        Toast.makeText(this, "Saved Array List to Shared preferences. ", Toast.LENGTH_SHORT).show();
    }

    /*
     * Sync button posts to API endpoint
     * Checks for network connection,
     * if Connected, posts responses that had been saved offline*/
    @Override
    public void onSyncClick(View v, int position) {
        SurveyMethods survey = sSurveys.get(position);
        String sID = survey.getsID();

        if (isConnected()) {
            /*
             * GET survey responses that had been saved to sqlite db based on the survey ID
             * */
            try {
                pDialog.setMessage("Syncing  Survey Questions");
                pDialog.setCancelable(false);
                showDialog();
                db.getAnswers(sID);

                HashMap<String, String> user = db.getUserDetails();
                String sSessionKey = user.get("keyToken");

                Log.d("answers from db", db.getAnswers(sID).toString());
                // post and sync surveys to server
                String POST_URL = " https://www.futuristickwtadev.com/index.php/api/add_responses?iSurveyID=" + sID + "&aResponseData=" + db.getAnswers(sID).get("answers").toString() + "&sSessionKey=" + sSessionKey;
                String tag_string_req = "req_questions";
                Log.d("tadds", POST_URL);

                StringRequest strReq = new StringRequest(Request.Method.GET, POST_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("submit answers res", "res" + response);
                            hideDialog();
                            Toast.makeText(getApplicationContext(), "Synced Successfully", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("q", "Q Error: " + "error endpoint");
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        // Posting parameters to login url
                        Map<String, String> params = new HashMap<String, String>();
                        //params.put("iSurveyID", sID);
                        //params.put("aResponseData", db.getAnswers(sID).get("answers").toString());
                        //params.put("iSurveyID", sID);
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

                RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                strReq.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(strReq);
            }catch(Exception ex){
                ex.printStackTrace();
                Log.d("Sync Error",ex.getMessage());
            }

            /*
             * If not connected,
             * make toast to notify the user*/
        } else if (!isConnected()) {
            pDialog.setMessage("Cannot Sync Without Network Connection, check network connection and try again");
            pDialog.setCancelable(false);
            showDialog();
            new android.os.Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    hideDialog();
                }
            }, 1000);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public boolean checkGpsStatus() {
        // see if gps id enabled
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return GpsStatus;
    }

    /*Check internet connection
     * Ping Googles DNS*/
    public boolean isConnected() {
        //Connectivity Code
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
            return true;
        } else
            connected = false;
        return false;



    }

    /*
     * Get all questions based on survey id onSaveClick
     * Save the questions locally with PK as the survey ID*/
    private void getAllQuestions(String sID, String sSessionKey) {
        String tag_string_req = "req_questions";

        String HTML_URL = "https://www.futuristickwtadev.com/index.php/api/list_questions_test/?iSurveyID="+sID;
        if(isConnected()){
            StringRequest strReq = new StringRequest(Request.Method.GET, HTML_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String fileContents = response;
                    try {
                        Toast.makeText(getApplicationContext(), "Connection detected, Updating cache ", Toast.LENGTH_LONG).show();
                        // convert to array
                        JSONArray jArr = new JSONArray(response.toString());
                        Log.d(TAG, "onResponse: "+jArr);
                        // save questions locally
                        db.addQuestions(response.toString(),sID);
                        hideDialog();
                    } catch (JSONException e) { e.printStackTrace(); }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("q", "Q Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    logoutUser();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    // Posting parameters to getQurl url
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(120 * 1000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add( strReq);
        }

    }
    /*
     * GET user permission to access their location via GPS
     * if allowed initialize the gps provider
     * if not connected prompt the user */
    public void getPermision(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        initService();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
    /*open settings if not connected
     * and prompt the user to give access to fine location*/
    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /*
     * Initialize location service, get the location via gps*/
    private void initService() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                // Toast.makeText(getApplicationContext(), "service ccddone", Toast.LENGTH_SHORT).show();
                mCurrentLocation = locationResult.getLastLocation();
                String longSaved = String.valueOf(locationResult.getLastLocation().getLongitude());
                String latSaved = String.valueOf(locationResult.getLastLocation().getLatitude());
                String altSaved = String.valueOf(locationResult.getLastLocation().getAltitude());
                // Log.d("cd", locationSaved);
                //db.deleteLocation();
//                db.addLocation(longSaved +";"+ latSaved +";"+altSaved);
                QuestionsMethods questionsMethods = new QuestionsMethods();
                questionsMethods.setLocation(longSaved+";"+latSaved);
                // Toast.makeText(getApplicationContext(), "Your Location Longitude" + mCurrentLocation.getLongitude() + "Latitude"+  mCurrentLocation.getLatitude(), Toast.LENGTH_LONG).show();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                // updateLocationUI();
                // showLastKnownLocation();
            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();

        startLocationUpdates();
    }
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener( new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        // Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();
                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                    }
                });
    }

}

