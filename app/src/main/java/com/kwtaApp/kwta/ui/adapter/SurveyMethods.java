package com.kwtaApp.kwta.ui.adapter;

import android.os.Parcel;
import android.os.Parcelable;

public class SurveyMethods implements Parcelable {
    private String sID;
    private String title;
    private String desc;
    private String timestamp;
    private String savedState;
    private String syncState;

    public SurveyMethods( String sID,String title, String desc, String timestamp, String savedState, String syncState) {
        this.sID = sID;
        this.title = title;
        this.desc = desc;
        this.timestamp = timestamp;
        this.savedState = savedState;
        this.syncState = syncState;
    }

    public SurveyMethods() {

    }
    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getsDesc() {
        return desc;
    }

    public void setsDesc(String desc) {
        this.desc = desc;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getSavedState() {
        return savedState;
    }

    public void setSavedState(String savedState) {

        this.savedState = savedState;
    }
    public String getSyncState() {
        return syncState;
    }

    public void setSyncState(String syncState) {
        this.syncState = syncState;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "sid='" + sID + '\'' +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", syncState='" + syncState + '\'' +
                ", savedState='" + savedState + '\'' +

                '}';
    }

    protected SurveyMethods(Parcel in) {
        sID = in.readString();
        title = in.readString();
        desc = in.readString();
        timestamp = in.readString();
        syncState = in.readString();
        savedState = in.readString();
    }

    public static final Creator<SurveyMethods> CREATOR = new Creator<SurveyMethods>() {
        @Override
        public SurveyMethods createFromParcel(Parcel in) {
            return new SurveyMethods(in);
        }

        @Override
        public SurveyMethods[] newArray(int size) {
            return new SurveyMethods[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sID);
        dest.writeString(title);
        dest.writeString(desc);
        dest.writeString(timestamp);
        dest.writeString(syncState);
        dest.writeString(savedState);
    }

}
