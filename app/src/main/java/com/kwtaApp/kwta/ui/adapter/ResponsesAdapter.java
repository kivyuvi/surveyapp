package com.kwtaApp.kwta.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kwtaApp.kwta.R;

import java.util.ArrayList;

public class ResponsesAdapter extends RecyclerView.Adapter<ResponsesAdapter.ViewHolder> {
    private ArrayList<ResponseMethods> responses = new ArrayList<>();
    private ResponsesAdapter.onGasListener monGasListener;


    public ResponsesAdapter(ArrayList<ResponseMethods> responses, ResponsesAdapter.onGasListener onGasListener) {
        this.responses = responses;
        this.monGasListener = onGasListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.saved_responses_list, parent, false);
        return new ViewHolder(view,monGasListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.surveyName.setText(responses.get(position).getsID());
    }

    @Override
    public int getItemCount() {
        return responses.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView surveyName, surveyCount;
        onGasListener onResListener;
        Button syncButton, clearButton;

        public ViewHolder(View itemView, onGasListener onResListener) {
            super(itemView);
            surveyName = itemView.findViewById(R.id.savedName);
            // surveyCount = itemView.findViewById(R.id.saved_by_name);

            clearButton = itemView.findViewById(R.id.response_clear_btn);
            syncButton = itemView.findViewById(R.id.response_sync_btn);

            clearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onResListener.onClearClick(v, getAdapterPosition());
                }
            });
            syncButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onResListener.onSyncClick(v, getAdapterPosition());
                }
            });
        }
    }

    public interface onGasListener {
        void onClearClick(View v, int position);
        void onSyncClick(View v, int position);

    }
}