package com.kwtaApp.kwta.ui.adapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.kwtaApp.kwta.CommonFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuestionsMethods implements Parcelable {
    private String sID;
    private String title;
    private String question;
    private String help;
    private String type;
    private String editTextValue;
    private String hugeTextValue;
    private String fileName;
    private String GPSlocation;
    private JSONObject c_values;
    private JSONObject arrayNumbers;
    private JSONObject arrayText;
    private JSONObject CV_alues;
    private ArrayList<String> answers;
    private ArrayList<String>cboxanswers;
    private ArrayList<String>pboxanswers;
    private ArrayList<String>type1answers;
    private ArrayList<String> typeCanswers;
    private ArrayList<String> typeAnswers;
    private ArrayList<String> typeGender;
    private ArrayList<String> typeEanswers;
    private ArrayList<String> typelist;
    private ArrayList<String>typeOlist;
    private  ArrayList<String> typecolonlist;
    private ArrayList<String> typesemiandcolonlist;
    private ArrayList<String> typeAanswers;



    public QuestionsMethods(String sID, String title, String question, String help, String type, String fileName, JSONObject c_values, JSONObject CV_alues, ArrayList<String> answers,ArrayList<String>cboxanswers ,ArrayList<String>type1answers ,
                            ArrayList<String>typeCanswers ,ArrayList<String>typeAnswers,ArrayList<String>pboxanswers,
                            ArrayList<String>typeGender,ArrayList<String>typecolonlist,ArrayList<String>typeOlist,ArrayList<String>typeEanswers,
                            ArrayList<String> typelist ,ArrayList<String>typesemiandcolonlist,
                            ArrayList<String>typeAanswers
    ) {
        this.sID = sID;
        this.title = title;
        this.question = question;
        this.help = help;
        this.type = type;
        this.fileName = fileName;
        this.c_values = c_values;
        this.CV_alues = CV_alues;
        this.answers = answers;
        this.cboxanswers=cboxanswers;
        this.pboxanswers=pboxanswers;
        this.type1answers=type1answers;
        this.typeCanswers=typeCanswers;
        this.typeAnswers=typeAnswers;
        this.typeGender=typeGender;
        this.typeEanswers=typeEanswers;
        this.typelist=typelist;
        this.typeOlist=typeOlist;
        this.typecolonlist=typecolonlist;
        this.typeAanswers=typeAanswers;
        this.typesemiandcolonlist=typesemiandcolonlist;
    }

    public QuestionsMethods() {

    }
    public String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getsQuestions() {

       //return question.replaceAll("\\<.*?\\>", "");
        //return question.replaceAll("[;<&>\\/:*?\"|&']","");
        return  stripHtml(question);

    }

    public void setsQuestion(String question) {
        this.question = question;
    }

    public String getHelp() {
        return stripHtml(help);


        //return help.replaceAll("[;<&>\\/:*?\"|&']","");

    }

    public void setHelp(String help) {
        this.help = help;
    }


    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type.toString();
    }

    public String getEditTextValue() {
        return editTextValue;
    }

    public String getHugeTextValue() {
        return hugeTextValue;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setEditTextValue(String editTextValue) {
        this.editTextValue = editTextValue;
    }

    public void setHugeTextValue(String hugeTextValue) {
        try {
            this.hugeTextValue = hugeTextValue;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public String getLocation() {
        return GPSlocation;
    }

    public void setLocation(String GPSlocation) {
        this.GPSlocation = GPSlocation;
    }

    //Checkbox questions with comment
    public String getCheckboxComment() {
        try {
            JSONArray jsonArray1 = new JSONArray(CV_alues);
            return jsonArray1.getString(0);

        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return "This is the checkbox with comment";
    }

    public String getCheckboxCommentOne() {
        String one = null;
        try {
            one = CV_alues.getString("one");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCheckboxCommentTwo() {
        String one = null;
        try {
            one = CV_alues.getString("two");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }

    public String getCheckboxCommentThree() {
        String one = null;
        try {
            one = CV_alues.getString("three");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }

    public void setCheckboxComment(JSONObject CV_alues) {
        this.CV_alues = CV_alues;
    }

    //checkbox questions
    public String getCheckbox() {
        try {
            JSONArray jsonArray = new JSONArray(c_values);
            return jsonArray.getString(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "hello";
    }

    public String getCheckboxOne() {
        String one = null;
        try {
            one = c_values.getString("one");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getCheckboxTwo() {
        String one = null;
        try {
            one = c_values.getString("two");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }

    public String getCheckboxThree() {
        String one = null;
        try {
            one = c_values.getString("three");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }


    public void setCheckbox(JSONObject c_values) {
        this.c_values = c_values;
    }

    /*
     * get array numbers*/

    public void setArrayNumbers(JSONObject arrayNumbers) {
        this.arrayNumbers = arrayNumbers;
    }

    public String getArrayOne() {
        String one = null;
        try {
            one = arrayNumbers.getString("one");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getArrayTwo() {
        String one = null;
        try {
            one = arrayNumbers.getString("two");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }

    public String getArrayThree() {
        String one = null;
        try {
            one = arrayNumbers.getString("three");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "one";
    }

    //text array questions
    public void setArrayText(JSONObject arrayText) {
        this.arrayText = arrayText;
    }

    public String getTextArrayOne() {
        String one = null;
        try {
            one = arrayText.getString("one");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTextArrayTwo() {
        String one = null;
        try {
            one = arrayText.getString("two");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTextArrayThree() {
        String one = null;
        try {
            one = arrayText.getString("three");
            JSONObject jsonObject = new JSONObject(one);
            String val = jsonObject.getString("question");
            return val;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Type L
    public String getRadioButton() {
        String one = null;
        try {
            // JSONArray jArray = new JSONArray(jObj.toString());


            JSONObject jsonObject = new JSONObject(one);
            String myJ = jsonObject.toString();

            if (myJ.contains("What rank were you while in your former job")) {
                CommonFunctions commonFunctions = new CommonFunctions();
                String abc = commonFunctions.getChildObject(myJ);
                // shareItem(abc);
                //Parses through the First Item



                JSONObject jobj1 = new JSONObject(abc);
                String namecbox = jobj1.getString("answer");
                return namecbox;


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }


    // return the array of numbers as is
    public String getWholeArr() {
        return arrayNumbers.toString();
    }

    @Override
    public String toString() {
        return "Survey{" +
                "sid='" + sID + '\'' +
                "title='" + title + '\'' +
                ", question='" + question + '\'' +
                ", help='" + help + '\'' +
                ", type='" + type + '\'' +
                ", fileName='" + fileName + '\'' +
                '}';
    }

    protected QuestionsMethods(Parcel in) {
        sID = in.readString();
        title = in.readString();
        question = in.readString();
        help = in.readString();
        type = in.readString();
        fileName = in.readString();
    }

    public static final Parcelable.Creator<QuestionsMethods> CREATOR = new Parcelable.Creator<QuestionsMethods>() {
        @Override
        public QuestionsMethods createFromParcel(Parcel in) {
            return new QuestionsMethods(in);
        }

        @Override
        public QuestionsMethods[] newArray(int size) {
            return new QuestionsMethods[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sID);
        dest.writeString(title);
        dest.writeString(question);
        dest.writeString(help);
        dest.writeString(type);
        dest.writeString(fileName);
    }


    public JSONObject getCValues() {
        return c_values;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }
    public ArrayList<String> getcboxAnswers() {
        return cboxanswers;
    }

    public void setcboxAnswers(ArrayList<String> cboxanswers) {
        this.cboxanswers = cboxanswers;
    }
    public  ArrayList<String>getPboxanswers(){return  pboxanswers;}

    public void setPboxanswers(ArrayList<String> pboxanswers) {
        this.pboxanswers = pboxanswers;
    }

    public ArrayList<String>getType1answers(){return type1answers;}
    public void setType1answers(ArrayList<String>type1answers){this.type1answers=type1answers;}
    public ArrayList<String>getTypeCanswers()
    {
        return typeCanswers;
    }

    public void setTypeCanswers(ArrayList<String> typeCanswers) {
        this.typeCanswers = typeCanswers;
    }
    public void setTypeAnswers(ArrayList<String>typeAnswers){
        this.typeAnswers=typeAnswers;
    }
    public ArrayList<String>getTypeAnswers(){return  typeAnswers;}

    public ArrayList<String> getTypeGender() {
        return typeGender;
    }

    public void setTypeGender(ArrayList<String> typeGender) {
        this.typeGender = typeGender;
    }
    public ArrayList<String>getTypeEanswers(){return  typeEanswers;}

    public void setTypeEanswers(ArrayList<String> typeEanswers) {
        this.typeEanswers = typeEanswers;
    }

    public void setTypelist(ArrayList<String> typelist) {
        this.typelist = typelist;
    }
    public ArrayList<String> getTypelist(){
        return  typelist;
    }
    public ArrayList<String> getTypeOlist(){
        return  typeOlist;
    }

    public void setTypeOlist(ArrayList<String> typeOlist) {
        this.typeOlist = typeOlist;
    }
    public ArrayList<String>getTypecolonlist(){
        return  typecolonlist;
    }
    public void setTypecolonlist(ArrayList<String>typecolonlist){
        this.typecolonlist=typecolonlist;
    }
    public ArrayList<String>getTypesemiandcolonlist(){
        return  typesemiandcolonlist;
    }
    public void setTypesemiandcolonlist(ArrayList<String>typecolonlist){
        this.typesemiandcolonlist=typesemiandcolonlist;
    }

    public ArrayList<String> getTypeAanswers() {
        return typeAanswers;
    }

    public void setTypeAanswers(ArrayList<String> typeAanswers) {
        this.typeAanswers = typeAanswers;
    }
}

