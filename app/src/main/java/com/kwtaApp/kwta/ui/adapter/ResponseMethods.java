package com.kwtaApp.kwta.ui.adapter;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseMethods implements Parcelable {
    private String ID;
    private String title;
    private  String help;

    public ResponseMethods( String ID, String title, String help) {

        this.ID = ID;
        this.title = title;
        this.help = help;
    }

    public ResponseMethods() {

    }
    public String getsID() {
        return ID;
    }

    public void setsID(String savedState) {
        this.ID = savedState;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    @Override
    public String toString() {
        return "Responses{" +
                ", syncState='" + title + '\'' +
                ", savedState='" + ID + '\'' +
                ", savedState='" + help + '\'' +

                '}';
    }
    protected ResponseMethods(Parcel in) {
        title = in.readString();
        ID = in.readString();
        help = in.readString();
    }

    public static final Creator<ResponseMethods> CREATOR = new Creator<ResponseMethods>() {
        @Override
        public ResponseMethods createFromParcel(Parcel in) {
            return new ResponseMethods(in);
        }

        @Override
        public ResponseMethods[] newArray(int size) {
            return new ResponseMethods[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(title);
        dest.writeString(help);
    }
}
