package com.kwtaApp.kwta.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kwtaApp.kwta.R;

import java.util.ArrayList;

public class  SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.ViewHolder> {
    private ArrayList<SurveyMethods> sSurveys = new ArrayList<>();
    private onGasListener monGasListener;





    public SurveyAdapter(ArrayList<SurveyMethods> sSurveys, onGasListener onGasListener) {
        this.sSurveys = sSurveys;
        this.monGasListener = onGasListener;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gas_list_items, parent, false);
        return new ViewHolder(view,monGasListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.save.setText(sSurveys.get(position).getSavedState());
        holder.timestamp.setText("View");
        holder.title.setText(sSurveys.get(position).getTitle());
        holder.desc.setText(sSurveys.get(position).getsDesc());
    }

    @Override
    public int getItemCount() {

        return sSurveys.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button timestamp, save, sync;
        TextView  title, desc;
        onGasListener onGasListener;

        public ViewHolder(View itemView, onGasListener onGasListener) {
            super(itemView);
            timestamp = itemView.findViewById(R.id.note_timestamp);
            sync = itemView.findViewById(R.id.sync_btn);
            save = itemView.findViewById(R.id.save_btn);
            title = itemView.findViewById(R.id.note_title);
            desc = itemView.findViewById(R.id.note_desc);

            timestamp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGasListener.onGasClick(v,getAdapterPosition());
                }
            });
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onGasListener.onSaveClick(v,getAdapterPosition());
                }
            });
            sync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGasListener.onSyncClick(v,getAdapterPosition());
                }
            });

        }

    }
    public interface onGasListener{
        void onGasClick(View v, int position);

        void onSaveClick(View v, int position);

        void onSyncClick(View v, int position);
    }
}
