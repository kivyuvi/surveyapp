package com.kwtaApp.kwta.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.kwtaApp.kwta.R;
import com.kwtaApp.kwta.SurveyQuestions;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.adapter.QuestionsAdapter;
import com.kwtaApp.kwta.ui.adapter.QuestionsMethods;
import com.kwtaApp.kwta.ui.adapter.SurveyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static com.kwtaApp.kwta.MainActivity.SURVEY_METHOD;


public class AnswerOptions extends AppCompatActivity {
    private SQLiteHandler db;
    private ArrayList<QuestionsMethods> qQuestions = new ArrayList<>();
    private QuestionsMethods questionsMethods;

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public String getDetails(){
        String a="Junior developer";
        String b="Senior developer";
        String c="Manager";
        String d="Trainee";
        return a;

    }

    public String getAnswers() {
        String answer="";
        try {
            //get survey methods and get active survey id
            SurveyMethods survey = getIntent().getParcelableExtra(SURVEY_METHOD);
            String survey_id = survey.getsID();
            //get questions based on the survey id
            HashMap<String, String> questions = db.getQuestions(survey_id);

            Log.d("dis", "qi" + questions.get("question"));
            ArrayList<String> listQuestions = new ArrayList<String>();
            Object jObj = questions.get("question").toString();

            JSONArray jArray = new JSONArray(jObj.toString());
            if (jArray != null) {
                for (int i = 0; i < jArray.length(); i++) {
                    listQuestions.add(jArray.getString(i));
                    //shareItem(jArray.getString(i));
                }
            }

            // loop through the response
            for (int i = 0; i < listQuestions.size(); i++) {
                questionsMethods = new QuestionsMethods();
                JSONObject jsonObject = jArray.getJSONObject(i);
                String myJ = jsonObject.toString();
                // shareItem(myJ);

                //if (myJ.contains("Which Programming Language do you prefer?")) {
                if (jsonObject.getString("type").equals("L")) {

                    Iterator<String> keys = jsonObject.keys();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        //Timber.d("Json Key: %s",key);
                        if (jsonObject.get(key) instanceof JSONObject) {
                            try {
                                JSONObject object = (JSONObject) jsonObject.get(key);

                                answer = object.get("answer").toString();
                                Toast.makeText(AnswerOptions.this, "This is it" + answer + "/n", Toast.LENGTH_LONG).show();
                                Log.d("SurveyQuestions Eerror", answer);

                                //return key.length() == 0 ? null : key;
                                break;






                                //btn1.append("1");
                                // shareItem(answer);




                            } catch (Exception ex) {
                                //shareItem(ex.getMessage());

                            }
                        }


                    }



                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return answer;
    }
}



