package com.kwtaApp.kwta.ui.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.kwtaApp.kwta.NotSyncedFragment;
import com.kwtaApp.kwta.SyncedFragment;

public class TabAdapter extends FragmentPagerAdapter {
    private Context myContext;
    int totalTabs;

    public TabAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                NotSyncedFragment notSyncedFragment = new NotSyncedFragment();
                return notSyncedFragment;
            case 1:
                SyncedFragment syncedFragment = new SyncedFragment();
                return syncedFragment;
            default:
                return null;
        }
    }
    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
