package com.kwtaApp.kwta.ui.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.MapView;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.kwtaApp.kwta.R;
import com.kwtaApp.kwta.SurveyQuestions;
import com.kwtaApp.kwta.helpers.SQLiteHandler;
import com.kwtaApp.kwta.ui.AnswerOptions;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import timber.log.Timber;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {
    private ArrayList<QuestionsMethods> qQuestions = new ArrayList<>();
    private QuestionsAdapter.onGasListener monGasListener;
    //Image request code
    private int PICK_FILE_REQUEST = 1;
    SQLiteHandler db;



    // int b=Integer.parseInt(a);
    //int d=Integer.parseInt(c);


    public QuestionsAdapter(ArrayList<QuestionsMethods> qQuestions, onGasListener onGasListener) {
        this.qQuestions = qQuestions;
        this.monGasListener = onGasListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.questions_list, parent, false);
        return new QuestionsAdapter.ViewHolder(view, monGasListener);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        SpannableString content = new SpannableString(qQuestions.get(position).getHelp());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);

       holder.question.setText(qQuestions.get(position).getsQuestions());
        holder.title.setText(qQuestions.get(position).getTitle());
        //holder.help.setText(qQuestions.get(position).getHelp());
        holder.help.setText(content);

        String radio = "5";
        try {
            if (qQuestions.get(position).getType().equals(radio)) {
                //Radio-list questions-D
                holder.radioGroup.setVisibility(View.VISIBLE);
                //holder.textInput.setText(qQuestions.get(position).getRadioButton());
            }
            if (qQuestions.get(position).getType().equals("T")) {
                //Long Free Questions-D
                holder.textInput.setVisibility(View.VISIBLE);
                holder.textInput.setText(qQuestions.get(position).getEditTextValue());
            } else {
                holder.textInput.setVisibility(View.GONE);
                holder.textInput.setText("");
            }
            if (qQuestions.get(position).getType().equals("!")) {

                //List DropDown Questions
                holder.textInput.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypelist();
               // Toast.makeText(holder.list_spinner.getContext(), "Type Spinner Current answer: " + answers, Toast.LENGTH_SHORT).show();
                for (int i = 0; i < answers.size(); i++) {
                    ListView l=(ListView)holder.list_spinner.getChildAt(i);
                    ArrayAdapter<String> arr=new ArrayAdapter<String>(holder.list_spinner.getContext() ,R.layout.listadditems,answers);
                   // Toast.makeText(holder.list_spinner.getContext(), "Type Spinner Arr : " + arr, Toast.LENGTH_SHORT).show();
                    if (answers.size() > i) {

                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        arr.add(answer);

                        l.setAdapter(arr);
                        Toast.makeText(holder.list_spinner.getContext(), "Type Spinner Current answer: " + l, Toast.LENGTH_SHORT).show();

                    }
                }

            } /*else {
                holder.list_spinner.setVisibility(View.GONE);
            }*/


            if (qQuestions.get(position).getType().equals("|")) {
                //File Upload Questions
                holder.file_upload.setVisibility(View.VISIBLE);
                holder.fileName.setText((qQuestions.get(position).getFileName()));
            } /*else {
                holder.file_upload.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("S")) {
                //Short Free text-D
                holder.textShortInput.setVisibility(View.VISIBLE);
                //holder.coordinates.setVisibility(View.VISIBLE);
                //holder.coordinates.setText(qQuestions.get(position).getLocation());
                Log.d("mfs", db.getLocation().toString());

            } /*else {
                holder.textShortInput.setVisibility(View.GONE);
                holder.coordinates.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("M")) {
                //CheckBox questons-D
                holder.checkbox.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getcboxAnswers();
                //Toast.makeText(holder.checkbox.getContext(), "Type M Current answer: " + answers, Toast.LENGTH_SHORT).show();
                for (int i = 0; i < 5; i++) {
                    CheckBox checkboxM = (CheckBox) holder.checkbox.getChildAt(i);

                    if (answers.size() > i) {

                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        checkboxM.setText(answer);
                    }
                }
            }

            if (qQuestions.get(position).getType().equals("P")) {
                //CheckBox Qestions-D
                holder.checkboxtypep.setVisibility(View.VISIBLE);
                // holder.checkboxtypep.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getPboxanswers();
                //Toast.makeText(holder.checkboxtypep.getContext(),"P answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.checkboxtypep.getChildCount(); i++) {
                    CheckBox radioButton = (CheckBox) holder.checkboxtypep.getChildAt(i);
                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        radioButton.setText(answer);
                    }
                }


            }
            if (qQuestions.get(position).getType().equals("F")) {
                //Array Type Questions:6-I likely Questions-D
                holder.typefradio.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("L")) {
                //List radio Questions-D
                holder.typeLradio.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getAnswers();
                //Toast.makeText(holder.typeLradio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.typeLradio.getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) holder.typeLradio.getChildAt(i);
                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        radioButton.setText(answer);

                    }

                }


            } /*else {
                holder.bootstrap.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("O")) {
                //list with Comment Questions-P
                holder.listwithcomment.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeOlist();
                //Toast.makeText(holder.typeLradio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.listwithcomment.getChildCount(); i++) {


                    if (answers.size() > i) {
                        int len1=answers.size();

                        String ans2=answers.get(len1-4);
                        String ans3=answers.get(len1-3);
                        String ans4=answers.get(len1-2);
                        String ans5=answers.get(len1-1);
                        String answer = answers.get(0);
                        Timber.d("Current answer: %s", answer);

                        holder.TL1.setText(answer);
                        holder.TL2.setText(ans2);
                        holder.TL3.setText(ans3);
                        holder.TL4.setText(ans4);

                    }
                }
            } /*else {
                holder.listwithcomment.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("Y")) {
                //Yes/No Questions
                holder.yesNoRadio.setVisibility(View.VISIBLE);
            } else {
                holder.yesNoRadio.setVisibility(View.GONE);
            }
            if (qQuestions.get(position).getType().equals(":")) {
                holder.arrayNumberLayout.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypecolonlist();
                //Toast.makeText(holder.typeLradio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.arrayNumberLayout.getChildCount(); i++) {
                    //RadioButton radioButton = (RadioButton) holder.typeLradio.getChildAt(i);
                    if (answers.size() > i) {
                        int len1=answers.size();
                        String ans1=answers.get(len1-2);
                        String ans2=answers.get(len1-1);
                        String ans3=answers.get(0);
                        Timber.d("Current answer: %s", answers);
                      holder.arrNum1.setText(ans1);
                      holder.arrNum2.setText(ans2);
                      holder.arrNum3.setText(ans3);
                    }
                }

            } /*else {
                holder.arrayNumberLayout.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals(":;")) {
                //Arrays&textQuestions-D
                holder.arrayTextLayout.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypesemiandcolonlist();
                //Toast.makeText(holder.typeLradio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.arrayTextLayout.getChildCount(); i++) {
                    //RadioButton radioButton = (RadioButton) holder.typeLradio.getChildAt(i);
                    if (answers.size() > i) {
                        int len1=answers.size();
                        String ans1=answers.get(len1-2);
                        String ans2=answers.get(len1-1);
                        String ans3=answers.get(0);
                        Timber.d("Current answer: %s", answers);
                        holder.arrText1.setText(ans1);
                        holder.arrText2.setText(ans2);
                        holder.arrText3.setText(ans3);
                    }
                }

            }/* else {
                holder.arrayTextLayout.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("C")) {
                //Yes/No/UnCertain Questions-Table Like-Done
                holder.typecradio.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeCanswers();
                //Toast.makeText(holder.typecradio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.typecradio.getChildCount(); i++) {
                    TextView radioButton = (TextView) holder.typecradio.getChildAt(i);
                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        int len1=answers.size();
                        String ans1=answers.get(len1-4);
                        String ans2=answers.get(len1-3);
                        String ans3=answers.get(len1-2);
                        String ans4=answers.get(len1-1);
                        String ans5=answers.get(0);
                        // Toast.makeText(holder.typecradio.getContext(),"Type C Current answer: "+answers.size() ,Toast.LENGTH_SHORT).show();
                        Timber.d("Type C Current answer: %s", answer);
                        //  radioButton.setText(answer);
                        holder.T1C.setText(ans1);
                        holder.T2C.setText(ans2);
                        holder.T3C.setText(ans3);
                        holder.T4C.setText(ans4);
                        holder.T5C.setText(ans5);

                    }
                }
            } /*else {
                holder.typeC.setVisibility(View.GONE);
            }*/
            if (qQuestions.get(position).getType().equals("G")) {
                Log.d(QuestionsAdapter.class.getSimpleName(), "gender available");
                //Gender Questions-Done
                holder.genderRadio.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeGender();
                //Toast.makeText(holder.genderRadio.getContext(),"Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.genderRadio.getChildCount(); i++) {
                    TextView radioButton = (TextView) holder.genderRadio.getChildAt(i);
                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Type G Current answer: %s", answer);
                        radioButton.setText(answer);

                    }
                }

            }
            if (qQuestions.get(position).getType().equals("B")) {
                //Type BQuestions-Done
                holder.arrayb.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeGender();
                //Toast.makeText(holder.arrayb.getContext(),"Type B Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.arrayb.getChildCount(); i++) {
                    TextView radioButton = (TextView) holder.arrayb.getChildAt(i);
                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Type B Current answer: %s", answer);
                        radioButton.setText(answer);

                    }
                }

            }
            if (qQuestions.get(position).getType().equals("N")) {
                // Numerical InPut-Done
                holder.numericInput.setVisibility(View.VISIBLE);
                holder.numericInput.setText(qQuestions.get(position).getEditTextValue());
            } else {
                holder.numericInput.setVisibility(View.GONE);
                holder.numericInput.setText("");
            }
            if (qQuestions.get(position).getType().equals("U")) {
                //Huge free text Questions-Done
                holder.hugeText.setVisibility(View.VISIBLE);
                holder.hugeText.setText(qQuestions.get(position).getEditTextValue());
            } else {
                holder.hugeText.setVisibility(View.GONE);
                holder.hugeText.setText("");
            }
            if (qQuestions.get(position).getType().equals("A")) {
                //Array (5 point Choice)
                holder.typea.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeAanswers();
                // Toast.makeText(holder.type1radio.getContext()," Type 1 Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.typea.getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) holder.typea.getChildAt(i);

                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        radioButton.setText(answer);
                    }
                }


            }
            if (qQuestions.get(position).getType().equals("1")) {
                //Db//languages Questions-Done
                holder.type1radio.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getType1answers();
               // Toast.makeText(holder.type1radio.getContext()," Type 1 Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.type1radio.getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) holder.type1radio.getChildAt(i);

                    if (answers.size() > i) {
                        String answer = answers.get(i);
                        Timber.d("Current answer: %s", answer);
                        radioButton.setText(answer);
                    }
                }

            }
            if (qQuestions.get(position).getType().equals("E")) {
                //Increase/Decrease/Same Questions-Done
                holder.typeE.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeEanswers();
                //Toast.makeText(holder.typeE.getContext()," Type E Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.typeE.getChildCount(); i++) {
                    //RadioButton radioButton = (RadioButton) holder.typeE.getChildAt(i);

                    if (answers.size() > i) {

                        int len1=answers.size();
                        String ans1 = answers.get(len1-4);
                        String ans2 = answers.get(len1-3);
                        String ans3 = answers.get(len1-2);
                        String ans4 = answers.get(len1-1);
                        String ans5 = answers.get(0);
                        //Toast.makeText(holder.typeE.getContext()," The length of Type E Current answer: "+answers.size() ,Toast.LENGTH_SHORT).show();
                        Timber.d("Current answer: %s", ans1);
                        // radioButton.setText(answer);
                        holder.T1.setText(ans1);
                        holder.T2.setText(ans2);
                        holder.T3.setText(ans3);
                        holder.T4.setText(ans4);
                        holder.T5.setText(ans5);
                    }
                }
            }
            if (qQuestions.get(position).getType().equals("Q")) {
                //Input on Demand Questions
                holder.textInput.setVisibility(View.VISIBLE);
                // holder.textInput.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("H")) {
                //By Column Questions:Array Questions-5 Likely Questions-Done

                holder.typeH.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("::")) {
                //Array Type  Numbers
                holder.textInput.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals(";")) {
                //Array Type Texts-Done
                holder.listit.setVisibility(View.VISIBLE);
                ArrayList<String> answers = qQuestions.get(position).getTypeAnswers();
               // Toast.makeText(holder.listit.getContext()," Type ; Current answer: "+answers ,Toast.LENGTH_SHORT).show();
                for (int i = 0; i < holder.listit.getChildCount(); i++) {
                    TextView radioButton = (TextView) holder.listit.getChildAt(i);

                    if (answers.size() > i) {
                        int length1=answers.size();

                        String answer = answers.get(length1-1);
                       // Toast.makeText(holder.listit.getContext(),"The length of type ; is "+length1,Toast.LENGTH_SHORT).show();
                        String answer1 = answers.get(length1-8);
                        String answer2=answers.get(length1-7);
                        String answer3=answers.get(length1-6);
                        String answer4=answers.get(length1-5);
                        String answer5=answers.get(length1-4);
                        String answer6=answers.get(length1-3);
                        String answer7=answers.get(length1-2);
                        String answer8=answers.get(length1-1);
                        String answer9=answers.get(0);

                        Timber.d(" Type ; Current answer: %s", answer);
//
                        holder.T11A.setText(answer1);
                        holder.T12A.setText(answer2);
                        holder.T13A.setText(answer3);
                        holder.T14A.setText(answer4);
                        holder.T15A.setText(answer5);
                        holder.T16A.setText(answer6);
                        holder.T17A.setText(answer7);
                        holder.T18A.setText(answer8);
                        holder.T19A.setText(answer9);

                    }
                }

            }
            if (qQuestions.get(position).getType().equals("Q")) {
                //
                holder.inputDemand.setVisibility(View.VISIBLE);
            }
            //Mask Questions
            if (qQuestions.get(position).getType().equals("D")) {
                holder.date1.setVisibility(View.VISIBLE);
                final Calendar myCalendar = Calendar.getInstance();


                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "MM/dd/yy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        holder.date1.setText(sdf.format(myCalendar.getTime()));


                        //updateLabel();
                    }

                };


                holder.date1.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(v.getContext(), date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });
                holder.date1.setText(qQuestions.get(position).getEditTextValue());
            } else {
                holder.date1.setVisibility(View.GONE);
                holder.date1.setText("");
                //Date Questions
                //holder.datepicker.setVisibility(View.VISIBLE);

            }
            if (qQuestions.get(position).getType().equals("*")) {
                //Equation Type Questions
                holder.textInput.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("K")) {
                //Multiple Numerical Type questions
                holder.typeK.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("R")) {
                //Ranking Questions
                holder.textInput.setVisibility(View.VISIBLE);
            }
            if (qQuestions.get(position).getType().equals("X")) {
                //Text Display Questions
                holder.typeX.setVisibility(View.VISIBLE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }



    @Override
    public int getItemCount() {
        return qQuestions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button timestamp, save, sync, file_upload_btn, fButton, mButton, noButton;
        TextView title, help, question, fileName, coordinates;
        QuestionsAdapter.onGasListener onGasListener;
        RadioGroup radioGroup, genderRadio, yesNoRadio, typeLradio,type1radio ,typecradio ,typefradio;
        RadioGroup typea ,typeE ,typeH;
        LinearLayout checkbox;
        LinearLayout checkboxtypep;
        TextView T1,T2,T3,T4,T5;
        TextView TL1,TL2,TL3,TL4,TL5;
        TextView T1C,T2C,T3C,T4C,T5C,T11A ,T12A,T13A,T14A,T15A,T16A,T17A,T18A,T19A,T20A,T21A;
        Spinner list_spinner;
        EditText numericInput, textInput, textShortInput, hugeText;
        DatePicker datepicker;
        LinearLayout bootstrap, inputDemand,listit, genderlay;
        ListView ranking;
        CheckBox cBox1p,cBox2p,cBox3p;
        //Checkbox
        CheckBox cBox1, cBox2, cBox3;
        RadioButton rbtn1,rbtn2,rbtn3,rbtn4;
        //arr numbers
        TextView arrNum1, arrNum2, arrNum3;
        LinearLayout arrayNumberLayout, genderButton, listwithcomment;
        //arr text
        TextView arrText1, arrText2, arrText3, typeX;
        LinearLayout arrayTextLayout;

        TextView TN1A,TN2A,TN3A,TN4A;


        TableLayout arrayb, typeK;
        EditText date1;


        View file_upload;

        public ViewHolder(View itemView, QuestionsAdapter.onGasListener onGasListener) {
            super(itemView);

            title = itemView.findViewById(R.id.singleQuestion);
            question = itemView.findViewById(R.id.some_question);
            help = itemView.findViewById(R.id.some_help_text);
            //TextView(Increase/Decrease/Same)
            T1=itemView.findViewById(R.id.txtInc1);
            T2=itemView.findViewById(R.id.txtInc2);
            T3=itemView.findViewById(R.id.txtInc3);
            T4=itemView.findViewById(R.id.txtInc4);
            T5=itemView.findViewById(R.id.txtInc5);
            //TextView(Yes/No/Uncertain)
            T1C=itemView.findViewById(R.id.txttypeC1);
            T2C=itemView.findViewById(R.id.txttypeC2);
            T3C=itemView.findViewById(R.id.txttypeC3);
            T4C=itemView.findViewById(R.id.txttypeC4);
            T5C=itemView.findViewById(R.id.txttypeC5);

            //List With Comments
            TL1=itemView.findViewById(R.id.txtlistcom1);
            TL2=itemView.findViewById(R.id.txtlistcom2);
            TL3=itemView.findViewById(R.id.txtlistcom3);
            TL4=itemView.findViewById(R.id.txtlistcom4);
            TL5=itemView.findViewById(R.id.txtlistcom5);

            //EditTextViewAnswers
            T11A=itemView.findViewById(R.id.txttypeit1);
            T12A=itemView.findViewById(R.id.txttypeit12);
            T13A=itemView.findViewById(R.id.txttypeit13);
            T14A=itemView.findViewById(R.id.txttypeit14);
            T15A=itemView.findViewById(R.id.txttypeit15);
            T16A=itemView.findViewById(R.id.txttypeit16);
            T17A=itemView.findViewById(R.id.txttypeit17);
            T18A=itemView.findViewById(R.id.txttypeit18);
            T19A=itemView.findViewById(R.id.txttypeit19);
            T20A=itemView.findViewById(R.id.txttypeit20);
            T21A=itemView.findViewById(R.id.txttypeit21);

            //Type A
            typea=itemView.findViewById(R.id.rdlayouttypea);
            TN1A=itemView.findViewById(R.id.txtA1);
            TN2A=itemView.findViewById(R.id.txtA2);
            TN3A=itemView.findViewById(R.id.txtA3);
            TN4A=itemView.findViewById(R.id.txtA4);



            //q types
            rbtn1=itemView.findViewById(R.id.radiatypeL1);
            rbtn2=itemView.findViewById(R.id.radiatypeL12);
            rbtn3=itemView.findViewById(R.id.radiatypeL13);
            rbtn4=itemView.findViewById(R.id.radiatypeL14);

            date1=itemView.findViewById(R.id.Birthday);
            radioGroup = itemView.findViewById(R.id.groupradio);
            listwithcomment = itemView.findViewById(R.id.listwithcomment);
            typeLradio=itemView.findViewById(R.id.typeLradio);
            typecradio=itemView.findViewById(R.id.rdlayouttypec);
            typeE=itemView.findViewById(R.id.rdlayouttypeinc);
            typeH=itemView.findViewById(R.id.rdlayouttypeh);


            list_spinner = itemView.findViewById(R.id.spinner);
            genderRadio = itemView.findViewById(R.id.genderRadio);
            yesNoRadio = itemView.findViewById(R.id.yesNoRadio);
            checkbox = itemView.findViewById(R.id.checkbox_layout);
            checkboxtypep=itemView.findViewById(R.id.checkboxtypep_layout);
            numericInput = itemView.findViewById(R.id.inputNumber);
            textInput = itemView.findViewById(R.id.textInput);
            textShortInput = itemView.findViewById(R.id.textShortInput);
            coordinates = itemView.findViewById(R.id.coordinates);
            //datepicker=itemView.findViewById(R.id.date);

            file_upload = itemView.findViewById(R.id.file_upload);
            sync = itemView.findViewById(R.id.file_upload_btn);
            fileName = itemView.findViewById(R.id.file_name);

            hugeText = itemView.findViewById(R.id.editHugeText);
            inputDemand = itemView.findViewById(R.id.inputDemand);

            arrayb = itemView.findViewById(R.id.arrayb);



            type1radio=itemView.findViewById(R.id.rdlayouttype1);
            typefradio=itemView.findViewById(R.id.rdlayouttypef);

            typeK = itemView.findViewById(R.id.typeK);

            ranking = itemView.findViewById(R.id.list);
            typeX = itemView.findViewById(R.id.txtTypeX);
            listit=itemView.findViewById(R.id.layoutlist);

            //array numbers
            arrNum1 = itemView.findViewById(R.id.arraytxt1);
            arrNum2 = itemView.findViewById(R.id.arraytxt2);
            arrNum3 = itemView.findViewById(R.id.arraytxt3);
            arrayNumberLayout = itemView.findViewById(R.id.arrayNumLayout);





            //array text
            arrText1 = itemView.findViewById(R.id.arrayNtxt1);
            arrText2 = itemView.findViewById(R.id.arrayNtxt2);
            arrText3 = itemView.findViewById(R.id.arrayNtxt3);
            arrayTextLayout = itemView.findViewById(R.id.arrayTextLayout);

            // checkboxVal = itemView.findViewById(R.id.checkbox_values);
            cBox1 = itemView.findViewById(R.id.checkBox);
            cBox2 = itemView.findViewById(R.id.checkBox2);
            cBox3 = itemView.findViewById(R.id.checkBox3);


            cBox1p=itemView.findViewById(R.id.checkBox1typep);
            cBox2p=itemView.findViewById(R.id.checkBox2typep);
            cBox3p=itemView.findViewById(R.id.checkBox3typep);


            //
            file_upload_btn = itemView.findViewById(R.id.file_upload_btn);
            // file_upload_btn.setOnClickListener(new View.OnClickListener() {
            //     @Override
            //     public void onClick(View v) {
            //         Log.d("clicked", "clicked");
            //     }
            // });

            sync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGasListener.onSyncClick(v, getAdapterPosition());
                }

            });


            textInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    qQuestions.get(getAdapterPosition()).setEditTextValue(textInput.getText().toString());
                    //textInput.getText().clear();
//                    qQuestions.get(getAdapterPosition()).setHugeTextValue(hugeText.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {


                }
            });

        }

    }


    public interface onGasListener {
        void onGasClick(View v, int position);

        void onCheckClick(View v, int position);

        void onSyncClick(View v, int position);
    }

    public String getQType(String type, ViewHolder holder) {
        if (type == "T") {
            Log.d("true", "true");
        } else if (type == "G") {
            Log.d("true", "true");
        } else if (type == "5") {
            Log.d("true", "true");
        }

        Log.d("true", "false");
        return type;
    }


}